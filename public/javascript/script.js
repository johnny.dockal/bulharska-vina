// after page loads
$(document).ready(function () {
    //Init Fancybox
    $(".fancybox").fancybox();
    
    doScroll();

    // nicer select box by jquery
    $('select.nice').niceSelect();

    // news box
    $("#newsbox").click(function () {
        showNews();
    });
    $("#newsbox-hide").click(function () {
        hideNews(false);
    });

    // combos
    wine_type = $('#wine_type').siblings('div.nice').children('ul.list').children('li.selected');
    wine_variety = $('#wine_variety').siblings('div.nice').children('ul.list').children('li.selected');
    wine_viticulture = $('#wine_viticulture').siblings('div.nice').children('ul.list').children('li.selected');

    // change in 1st selectbox
    $('#wine_type').on('change', function () {

        // selects
        wine_type = $('#wine_type').siblings('div.nice').children('ul.list').children('li.selected');
        wine_variety = $('#wine_variety').siblings('div.nice').children('ul.list').children('li.selected');
        wine_viticulture = $('#wine_viticulture').siblings('div.nice').children('ul.list').children('li.selected');

        // values
        v_wine_type = wine_type.data("value");
        v_wine_variety = wine_variety.data("value");
        v_wine_viticulture = wine_viticulture.data("value");

        reloadCombobox(2, v_wine_viticulture, v_wine_type, 'wine_variety');
        reloadCombobox(3, v_wine_variety, v_wine_type, 'wine_viticulture');
        // });
    });

    // change in 2nd selectbox
    $('#wine_variety').on('change', function () {

        // selects
        wine_type = $('#wine_type').siblings('div.nice').children('ul.list').children('li.selected');
        wine_variety = $('#wine_variety').siblings('div.nice').children('ul.list').children('li.selected');
        wine_viticulture = $('#wine_viticulture').siblings('div.nice').children('ul.list').children('li.selected');

        // values
        v_wine_type = wine_type.data("value");
        v_wine_variety = wine_variety.data("value");
        v_wine_viticulture = wine_viticulture.data("value");

        reloadCombobox(1, v_wine_viticulture, v_wine_variety, 'wine_type');
        reloadCombobox(3, v_wine_type, v_wine_variety, 'wine_viticulture');
    });

    // change in 3rd selectbox
    $('#wine_viticulture').on('change', function () {

        // selects
        wine_type = $('#wine_type').siblings('div.nice').children('ul.list').children('li.selected');
        wine_variety = $('#wine_variety').siblings('div.nice').children('ul.list').children('li.selected');
        wine_viticulture = $('#wine_viticulture').siblings('div.nice').children('ul.list').children('li.selected');

        // values
        v_wine_type = wine_type.data("value");
        v_wine_variety = wine_variety.data("value");
        v_wine_viticulture = wine_viticulture.data("value");

        // change first option text
        // if (v_wine_viticulture == 'default') {
        // wine_viticulture.find("option[data-category='default']").html('Vinařství');
        // } else {
        // wine_viticulture.find("option[data-category='default']").html('Vše');
        // }

        reloadCombobox(1, v_wine_variety, v_wine_viticulture, 'wine_type');
        reloadCombobox(2, v_wine_type, v_wine_viticulture, 'wine_variety');
    });

    // reload all 3
    reloadCombobox(1, wine_viticulture.data("value"), wine_variety.data("value"), 'wine_type');
    reloadCombobox(2, wine_viticulture.data("value"), wine_type.data("value"), 'wine_variety');
    reloadCombobox(3, wine_variety.data("value"), wine_type.data("value"), 'wine_viticulture');

    // scroll to top of page by clicking footer arrow
    $("#totop").click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 2000);
        // prevent <a> link to continue 
        return false;
    });

    // open dialog form email sign off
    $("#mail-signoff").click(function () {
        showSignOff();
        return false;
    });

    // hide dialog form email sign off
    $("#overlay-form-signoff-close").click(function () {
        hideSignOff();
        return false;
    });

    // submit form for delete email from db
    $("#form-signoff-button").click(function () {
        var req = $.ajax({
            url: '/ajax/emailsignoff',
            type: 'POST',
            data: {
                address: $("#form-signoff-mail").val()
            }
        });

        req.done(function (data) {
            $("#ajax-form-output").html(data.message);
            dimm();
            $("#overlay-form-info").show(0);
            $("#overlay-form-captcha").hide(0);

            setTimeout(function () {
                window.location.reload();
            }, 1500);
        });

        return false;
    });

    // open dialog form email sign off
    $("#mailingformsubmit").click(function () {
        if (validateEmail($("#address").val())) {
            $("#address").removeClass('border-red');
            showCaptcha();
        } else {
            $("#address").addClass('border-red');
        }
        return false;
    });

    // confirm captcha dialog
    $("#overlay-form-captcha #captchasubmit").click(function () {
        formElement = $("#captchaform");
        $("#captchaaddress").val($("#address").val());
        confirmCaptcha(formElement);
        return false;
    });

    // hide dialog form email sign off
    $("#overlay-form-captcha-close").click(function () {
        hideCaptcha();
        return false;
    });

    // hide dialog form email sign off
    $("#overlay-form-info-close").click(function () {
        // undimm();
        $("#overlay-form-info").hide(0);
        return false;
    });

    // submit form for delete email from db
    $("#form-captcha-button").click(function () {
        console.log('button2');
        var req = $.ajax({
            url: '/ajax/emailsignin',
            type: 'POST',
            data: {
                address: $("#form-captcha-mail").val(),
                captcha: $("#form-captcha-mail").val()
            }
        });

        req.done(function (data) {
            $('#captcha-output').html(data.message);

            if (data.code == 0) {
                // console.log('email odebran');
                setTimeout(hideSignOff, 3000);
            } else {
                // console.log('chyba pri odebrani emailu');
            }
        });

        return false;
    });

    // price slider
    $slider = $("#slider-range").slider({
        range: true,
        min: 90,
        max: 500,
        values: [$("#amount-lower").val(), $("#amount-upper").val()],
        slide: function (event, ui) {
            // visible span
            $("#amount-bottom").html(ui.values[ 0 ] + " Kč");
            $("#amount-top").html(ui.values[ 1 ] + " Kč");
            // invisible form
            $("#amount-lower").val(ui.values[ 0 ]);
            $("#amount-upper").val(ui.values[ 1 ]);
        }
    });

    // open menu
    closeMenu = false;
    $(".dropdown").hover(
            function () {
                closeMenu = true;
                $(".dropdown-content").show(0);
                $(".dropdown h2").css('color', '#e0a00d');
            }, function () {
        setTimeout(function () {
            if (closeMenu) {
                $(".dropdown-content").hide(0);
                closeMenu = false;
            }
        }, 500);

    }
    );
    $(".dropdown-content").hover(
            function () {
                closeMenu = false;
                // $(".dropdown-content").show(0);
            }, function () {
        closeMenu = true;
        $(".dropdown-content").hide(0);
        $(".dropdown h2").css('color', '#580103');
    }
    );

    
    checkAge();
});

$(window).scroll(function () {
    doScroll();
});

//Functions

function doScroll() {
    var header = $('#header');

    // only for homepage
    if (!header.hasClass('header-big')) {
        return;
    }

    var filters = $('#menu');
    var content = $('#content');
    var scroll = $(window).scrollTop();

    if (scroll >= 224) {
        filters.addClass('fixed-menu');
        content.addClass('fixed-content');
        $('#menu-background').show();
        $('#slogan').hide(0);
        header.css('height', '136px');
    } else {
        $('#menu-background').hide();
        $('#slogan').show(0);
        filters.removeClass('fixed-menu');
        content.removeClass('fixed-content');
        header.css('height', (355 - scroll) + 'px');
    }
}

function reloadComboboxFinish(data, id) {
    div = $('#' + id);
    niceul = div.siblings('div.nice').children('ul');

    // hide all 
    div.find('option').css('display', 'none');
    niceul.find('li').css('display', 'none');

    // show only needed
    $(data).each(function () {
        c = $(this)[0].category;
        div.find("option[data-category='" + c + "']").css('display', 'block');
        niceul.find("li[data-value='" + c + "']").css('display', 'block');
    });

    // display first one - type
    niceul.find("li").first().css('display', 'block');
}

function reloadCombobox(group_id, subcatId1, subcatId2, id) {
    var req = $.ajax({
        url: '/ajax/getcategory',
        type: 'POST',
        data: {
            group_id: group_id,
            subcatId1: subcatId1,
            subcatId2: subcatId2
        }
    });

    req.done(function (msg) {
        reloadComboboxFinish(msg, id);
    });
}

function setNewsValue(show) {
    
    var showData = '0';
    if (show) {
        showData = '1';
    }

    var req = $.ajax({
        url: '/novinky/setnewsvalue',
        type: 'POST',
        data: {
            show: showData
        }
    });
}

function showNews() {
    contentWidth = $("#content").width();
    w = $('#header').width();
    new_w = ((w / 2) + 10);
    $("#newsbox-slideout").css('width', ((contentWidth / 2) - 10));
    $("#newsbox-slideout").animate({left: new_w + 'px'}, {queue: false, duration: 700});

    $("#newstitle").animate({left: '500px'}, {queue: false, duration: 700});

    $("#newsbox").hide(0);

    setNewsValue(true);
}

function hideNews(force) {
    if (force) {
        $("#newsbox-slideout").css({left: '150%'});
        $("#newsbox-slideout").show(0);

        $("#newstitle").css({left: '150%'});
        $("#newstitle").show(0);
    } else {
        $("#newsbox-slideout").animate({left: '100%'}, {queue: false, duration: 1000});

        $("#newstitle").animate({left: '200%'}, {queue: false, duration: 1000});
        setNewsValue(false);
    }
    $("#newsbox").show(0);
}

function confirmCaptcha(formElement) {
    // data = formElement.serialize();

    var req = $.ajax({
        url: '/ajax/emailsignin',
        type: 'POST',
        data: {
            captchaid: $('#captcha-id').val(),
            captchainput: $('#captcha-input').val(),
            address: $('#captchaaddress').val()
        }
    });

    req.done(function (data) {
        $("#ajax-form-output").html(data.message);

        dimm();
        $("#overlay-form-info").show(0);
        $("#overlay-form-captcha").hide(0);

        setTimeout(function () {
            window.location.reload();
        }, 1500);
    });
}

function reloadCartInfo(quantity, price) {
    $('#cart-num').text(quantity);
    $('#cart-sum').text(price);

    // v pripade, ze byl prazdny kosik a tohle je prvni polozka
    $('#cart-empty').hide(0);
    $('#cart-full').show(0);
}

function flyToCartAnimation(productId, fly) {
    cart = $('#cart');

    if (fly) {
        productCart = $('#product-sum-' + productId).parent().find('img').eq(0);

        hiddenCart = false;
        if (productCart.is(":visible")) {
            hiddenCart = true;
        }

        productCart.show(0);

        var flyingImage = productCart.clone()
                .offset({top: productCart.offset().top, left: productCart.offset().left})
                .css({'opacity': '0.8', 'position': 'absolute', 'z-index': '1001'})
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top,
                    'left': cart.offset().left
                }, 1000, 'easeInOutExpo');

        if (!hiddenCart) {
            productCart.hide(0);
        }

        flyingImage.animate({'width': 0, 'height': 0}, function () {
            $(this).detach()
        });
    }

    // shake the cart
    setTimeout(function () {
        cart.effect("shake", {
            times: 1,
            direction: "up",
            distance: 5
        }, 200);
    }, 1500);

}

function addProductToCart(productId, productPrice, quantity, fly) {
    flyToCartAnimation(productId, fly);

    var req = $.ajax({
        url: '/ajax/addproduct',
        type: 'POST',
        data: {
            product_id: productId,
            product_price: productPrice,
            product_quantity: quantity
        }
    });

    req.done(function (data) {
        // edit product count in span
        cnt = parseInt($('#product-sum-' + productId).text()) + parseInt(quantity);
        $('#product-sum-' + productId).text(cnt);
        $('#product-sum-' + productId).show(0);
        if ($('#product-sum-info-' + productId)) {
            $('#product-sum-info-' + productId).show(0);
        }

        reloadCartInfo(data.quantity, data.price);
    });
}

function showCaptcha() {
    dimm();
    $("#overlay-form-captcha").show(0);
}

function hideCaptcha() {
    undimm();
    $("#overlay-form-captcha").hide(0);
}

function showSignOff() {
    dimm();
    $("#overlay-form-signoff").show(0);
}

function hideSignOff() {
    undimm();
    $("#overlay-form-signoff").hide(0);
}

function showAllWines() {
    $("#all-wines-button").hide(0);
    $("#all-wines-holder").show(3000);
}

function dimm() {
    $("#overlay").show(0);
}

function undimm() {
    $("#overlay").hide(0);
}

function showDiffform(checkbox) {
    if (checkbox.checked) {
        $('#fieldset-diffform').show();
    } else {
        $('#fieldset-diffform').hide();
    }
}

function showComform(checkbox) {
    if (checkbox.checked) {
        //$('#fieldset-nameform').hide();
        $('#fieldset-companyform').show();
    } else {
        $('#fieldset-companyform').hide();
        //$('#fieldset-nameform').show();
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
};

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
};

function checkAge(){
    if(!readCookie('bulharskavina_age_verify')) {
        dimm();
        $('#overlay-age-verify').show(0);
    }
}

function confirmAge(){
    createCookie('bulharskavina_age_verify', 1, 365); // expire in 1 year
    undimm();
    $('#overlay-age-verify').hide(0);
}

function close_window() {
  if (confirm("Close Window?")) {
    close();
  }
}