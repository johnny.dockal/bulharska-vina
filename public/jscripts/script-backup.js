//Fancybox init
$(document).ready(function () {
    // chybi fancybox knihovna
    // $(".fancybox").fancybox();
});

function doScroll(){
    var header = $('#header');
  
    // only for homepage
    if(!header.hasClass('header-big')){
        return;
    }

    var filters = $('#menu');
    var content = $('#content');
    var scroll = $(window).scrollTop();

    if(scroll >= 224){
        filters.addClass('fixed-menu');
        content.addClass('fixed-content');
        $('#slogan').hide(0);
        header.css('height', '136px');
    }else{
        $('#slogan').show(0);
        filters.removeClass('fixed-menu');
        content.removeClass('fixed-content');
        header.css('height', (355-scroll)+'px');
    }
}

$(window).scroll(function(){
    doScroll();
});

function reloadComboboxFinish(data, id) {
    div = $('#' + id);

    // hide all 
    div.find('option').css('display', 'none');

    // show only needed
    $(data).each(function () {
        c = $(this)[0].category;
        div.find("option[data-category='" + c + "']").css('display', 'block');
    });

    // display default
    div.find("option[data-category='default']").css('display', 'block');

    // get first one selected if nothing 
    if (!div.val()) {
        var firstVisible = div.find('option').filter(function () {
            return $(this).css('display') == 'block';
        }).first().val();
        div.val(firstVisible);
    }
}

function reloadCombobox(group_id, subcatId1, subcatId2, id) {
    var req = $.ajax({
        url: '/ajax/getcategory',
        type: 'POST',
        data: {
            group_id: group_id,
            subcatId1: subcatId1,
            subcatId2: subcatId2
        }
    });

    req.done(function (msg) {
        reloadComboboxFinish(msg, id);
    });
}

function setNewsValue(show) {

    var showData = '0';
    if (show) {
        showData = '1';
    }

    var req = $.ajax({
        url: '/novinky/setnewsvalue',
        type: 'POST',
        data: {
            show: showData
        }
    });
}

function showNews() {
    contentWidth = $("#content").width();
    w = $('#header').width();
    new_w = ((w / 2) + 10);
    $("#newsbox-slideout").css('width', ((contentWidth / 2) - 10));
    $("#newsbox-slideout").animate({left: new_w + 'px'}, {queue: false, duration: 700});

    $("#newstitle").animate({left: '500px'}, {queue: false, duration: 700});

    $("#newsbox").hide(0);

    setNewsValue(true);
}

function hideNews(force) {
    if (force) {
        $("#newsbox-slideout").css({left: '150%'});
        $("#newsbox-slideout").show(0);

        $("#newstitle").css({left: '150%'});
        $("#newstitle").show(0);
    } else {
        $("#newsbox-slideout").animate({left: '100%'}, {queue: false, duration: 1000});

        $("#newstitle").animate({left: '200%'}, {queue: false, duration: 1000});
        setNewsValue(false);
    }
    $("#newsbox").show(0);
}

// after page loads
$(document).ready(function () {
    doScroll();
    
    // news box
    $("#newsbox").click(function () {
        showNews();
    });
    $("#newsbox-hide").click(function () {
        hideNews(false);
    });

    // combos
    var wine_type = $('#wine_type');
    var wine_variety = $('#wine_variety');
    var wine_viticulture = $('#wine_viticulture');

    // change in 1st selectbox
    wine_type.on('change', function () {

        // values
        v_wine_type = wine_type.val();
        v_wine_variety = wine_variety.val();
        v_wine_viticulture = wine_viticulture.val();

        // change first option text
        if (v_wine_type == 'default') {
            wine_type.find("option[data-category='default']").html('Typ');
        } else {
            wine_type.find("option[data-category='default']").html('Vše');
        }

        reloadCombobox(2, v_wine_viticulture, v_wine_type, 'wine_variety');
        reloadCombobox(3, v_wine_variety, v_wine_type, 'wine_viticulture');
    });

    // change in 2nd selectbox
    wine_variety.on('change', function () {

        // values
        v_wine_type = wine_type.val();
        v_wine_variety = wine_variety.val();
        v_wine_viticulture = wine_viticulture.val();

        // change first option text
        if (v_wine_variety == 'default') {
            wine_variety.find("option[data-category='default']").html('Odrůda');
        } else {
            wine_variety.find("option[data-category='default']").html('Vše');
        }

        reloadCombobox(1, v_wine_viticulture, v_wine_variety, 'wine_type');
        reloadCombobox(3, v_wine_type, v_wine_variety, 'wine_viticulture');
    });

    // change in 3rd selectbox
    wine_viticulture.on('change', function () {

        // values
        v_wine_type = wine_type.val();
        v_wine_variety = wine_variety.val();
        v_wine_viticulture = wine_viticulture.val();

        // change first option text
        if (v_wine_viticulture == 'default') {
            wine_viticulture.find("option[data-category='default']").html('Vinařství');
        } else {
            wine_viticulture.find("option[data-category='default']").html('Vše');
        }

        reloadCombobox(1, v_wine_variety, v_wine_viticulture, 'wine_type');
        reloadCombobox(2, v_wine_type, v_wine_viticulture, 'wine_variety');
    });

    // reload all 3
    reloadCombobox(1, wine_viticulture.val(), wine_variety.val(), 'wine_type');
    reloadCombobox(2, wine_viticulture.val(), wine_type.val(), 'wine_variety');
    reloadCombobox(3, wine_variety.val(), wine_type.val(), 'wine_viticulture');

    // scroll to top of page by clicking footer arrow
    $("#totop").click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 2000);
        // prevent <a> link to continue 
        return false;
    });
    
    // open dialog form email sign off
    $("#mail-signoff").click(function () {
        showSignOff();
        return false;
    });
    
    // hide dialog form email sign off
    $("#overlay-form-signoff-close").click(function () {
        hideSignOff();
        return false;
    });
    
    // submit form for delete email from db
    $("#form-signoff-button").click(function () {
        var req = $.ajax({
            url: '/ajax/emailsignoff',
            type: 'POST',
            data: {
                address: $("#form-signoff-mail").val()
            }
        });

        req.done(function (data) {
            $('#mailing-output').html(data.message);
            
            if(data.code == 0){
                // console.log('email odebran');
                setTimeout(hideSignOff, 3000);
            }else{
                // console.log('chyba pri odebrani emailu');
            }
        });
        
        return false;
    });
    
    // open dialog form email sign off
    $("#mailingformsubmit").click(function () {
        showCaptcha();
        return false;
    });
    
    // hide dialog form email sign off
    $("#overlay-form-captcha-close").click(function () {
        hideCaptcha();
        return false;
    });
    
    // submit form for delete email from db
    $("#form-captcha-button").click(function () {
        var req = $.ajax({
            url: '/ajax/emailsignin',
            type: 'POST',
            data: {
                address: $("#form-captcha-mail").val(),
                captcha: $("#form-captcha-mail").val()
            }
        });

        req.done(function (data) {
            $('#captcha-output').html(data.message);
            
            if(data.code == 0){
                // console.log('email odebran');
                setTimeout(hideSignOff, 3000);
            }else{
                // console.log('chyba pri odebrani emailu');
            }
        });
        
        return false;
    });


    // price slider
    $slider = $("#slider-range").slider({
        range: true,
        min: 90,
        max: 500,
        values: [$("#amount-lower").val(), $("#amount-upper").val()],
        slide: function (event, ui) {
            // visible span
            $("#amount-bottom").html(ui.values[ 0 ] + " Kč");
            $("#amount-top").html(ui.values[ 1 ] + " Kč");
            // invisible form
            $("#amount-lower").val(ui.values[ 0 ]);
            $("#amount-upper").val(ui.values[ 1 ]);
        }
    });
    
    // open menu
    closeMenu = false;
    $(".dropdown").hover(
        function() {
            closeMenu = true;
            $(".dropdown-content").show(0);
            $(".dropdown h2").css('color', '#e0a00d');
        }, function() {
            setTimeout(function () {
                if(closeMenu){
                    $(".dropdown-content").hide(0);
                    closeMenu = false;
                }
            }, 500);
            
        }
    );
    $(".dropdown-content").hover(
        function() {
            closeMenu = false;
            // $(".dropdown-content").show(0);
        }, function() {
            closeMenu = true;
            $(".dropdown-content").hide(0);
            $(".dropdown h2").css('color', '#580103');
        }
    );

});

function reloadCartInfo(quantity, price) {
    $('#cart-num').text(quantity);
    $('#cart-sum').text(price);

    // v pripade, ze byl prazdny kosik a tohle je prvni polozka
    $('#cart-empty').hide(0);
    $('#cart-full').show(0);
}

function flyToCartAnimation(productId, fly) {
    cart = $('#cart');

    if (fly) {
        productCart = $('#product-sum-' + productId).parent().find('img').eq(0);

        hiddenCart = false;
        if (productCart.is(":visible")) {
            hiddenCart = true;
        }

        productCart.show(0);

        var flyingImage = productCart.clone()
                .offset({top: productCart.offset().top, left: productCart.offset().left})
                .css({'opacity': '0.8', 'position': 'absolute', 'z-index': '1001'})
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top,
                    'left': cart.offset().left
                }, 1000, 'easeInOutExpo');

        if (!hiddenCart) {
            productCart.hide(0);
        }

        flyingImage.animate({'width': 0, 'height': 0}, function () {
            $(this).detach()
        });
    }

    // shake the cart
    setTimeout(function () {
        cart.effect("shake", {
            times: 1,
            direction: "up",
            distance: 5
        }, 200);
    }, 1500);

}

function addProductToCart(productId, productPrice, quantity, fly) {
    flyToCartAnimation(productId, fly);

    var req = $.ajax({
        url: '/ajax/addproduct',
        type: 'POST',
        data: {
            product_id: productId,
            product_price: productPrice,
            product_quantity: quantity
        }
    });

    req.done(function (data) {
        // edit product count in span
        cnt = parseInt($('#product-sum-' + productId).text()) + parseInt(quantity);
        $('#product-sum-' + productId).text(cnt);
        $('#product-sum-' + productId).show(0);
        if ($('#product-sum-info-' + productId)) {
            $('#product-sum-info-' + productId).show(0);
        }

        reloadCartInfo(data.quantity, data.price);
    });
}

function showCaptcha() {
    dimm();
    $("#overlay-form-captcha").show(0);
}

function hideCaptcha() {
    undimm();
    $("#overlay-form-captcha").hide(0);
}

function showSignOff() {
    dimm();
    $("#overlay-form-signoff").show(0);
}

function hideSignOff() {
    undimm();
    $("#overlay-form-signoff").hide(0);
}

function dimm() {
    $("#overlay").show(0);
}

function undimm() {
    $("#overlay").hide(0);
}
