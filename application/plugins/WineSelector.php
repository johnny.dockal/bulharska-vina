<?php

/**
 * Plugin zjištuje požadavky na změnu jazyka webu v parametru.
 * Pokud odpovídá českému nebo anglickému jazyku, nastaví ji do Zend session.
 * @author Daniel Vála
 */
class Plugin_WineSelector extends Zend_Controller_Action_Helper_Abstract {    

    public function preDispatch() {
        $model = new Model_DbTable_EshopGroups();
        $text = new Model_DbTable_Texts();
        $view = Zend_Layout::getMvcInstance()->getView();
        
        //předáme do view formulář pro přihlášení se k mailinglistu
        $form = new Form_Mailinglistform('/index/');
        $form->setDefault('redirect', Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());
        $view->mailingListForm = $form;
        
        //předáme do view formulář pro přihlášení se k mailinglistu
        $captchaform = new Form_Captchaform('/index/');
        //$form->setDefault('redirect', Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());
        $view->captchaform = $captchaform;
        
        //předáme poslední aktualitu
        $news = new Model_DbTable_News();
        $view->aktualita = $news->fetchNewsLatest();
        
        //předáme do view typy vína
        $wine_type = $model->fetchGroupById(1, true);
        $view->wine_type_default = $wine_type;
        $view->wine_types = $wine_type;
        $view->wine_varietys = $model->fetchGroupById(2, true);
        $view->wine_viticultures = $model->fetchGroupById(3, true);
    }
}
