<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of categories
 *
 * @author owner
 */
class Model_URLParser extends Zend_Db_Table_Abstract {

    protected $_name = 'categories';
    protected $_primary = 'category_id';
    protected $_sequence = true;
    protected $_langarray = array('cz', 'de', 'en');

    //procházení url
    function parseUrl($lang, $url) {
        $noSubCatCategories = array('4', '3', '7', '8', '9', '10', '11', '12', '17');
        trim($url);
        $data = array();
        //pokud je url prázdná, vyhodí stránku 404
        if ($url == "") {
            $data["page"] = "page404";
        } else {
            //načteme pole $urls - jednotlivé části url rozdělené dle lomítka (/)
            $urls = explode("/", $url);

            //rychlé ověření, zda je url reálné => pokud má více jak 3 části je v současné situaci neplatné => vyhodí stránku 404
            if (isset($urls[3])) {
                $data["page"] = "page404";
                return $data;
            }

            if ($lang == 'de') {
                $lang = 'cz';
            }

            //zjistíme, zda první část url vyhovuje kategorii
            $catModel = new Model_DbTable_EshopCategories();
            $category = $catModel->fetchCategoryByAlias($urls[0]);
            //pokud je dotaz do databáze prázdný, vyhodí stránku 404
            if (empty($category)) {
                $data["page"] = "page404";
                //v opačném případě načteme do pole $data potřebné údaje a pokračujeme v parsování url
            } else {
                $data["page"] = "category";
                $data["category_id"] = $category["category_id"];                
                $data["category"] = $category;
                $data["breadcrumbs_url"][] = $urls[0];
                $data["breadcrumbs_title"][] = $category["title"];

                if (isset($urls[1]) and in_array($category["category_id"], $noSubCatCategories)) {
                    $skipSubCat = true;
                } else {
                    $skipSubCat = false;
                }

                //pokud máme další část url
                if (isset($urls[1]) and ! $skipSubCat) {
                    //zjistíme, zda vyhovuje podkategorii a zároveň patří pod danou kategorii
                    $subcatModel = new Model_DbTable_EshopSubCategories();
                    $subcategory = $subcatModel->fetchSubCategoryByAlias($urls[1]);
                    //pokud není prázdná
                    if (!empty($subcategory)) {
                        $data["page"] = "subcategory";
                        $data["subcategory_id"] = $subcategory["subcategory_id"];
                        $data["subcategory"] = $subcategory;
                        $data["breadcrumbs_url"][] = $urls[1];
                        $data["breadcrumbs_title"][] = $subcategory["title"];
                    }
                }
                //pokud je další url, tak hledáme produkt v dané subkategorii
                if (isset($urls[2]) or $skipSubCat) {
                    //produkt musí spadat pod danou kategorii a subkategorii
                    $productModel = new Model_DbTable_EshopProducts();
                    if ($skipSubCat) {
                        $product_data = $productModel->fetchProductByAlias($urls[1]);
                        $url_add = $urls[1];
                    } else {
                        $product_data = $productModel->fetchProductByAlias($urls[2]);
                        $url_add = $urls[2];
                    }
                    $product = new Model_EshopProduct($product_data);
                    //pokud je produkt prázdný, vyhodí stránku 404
                    if (empty($product)) {
                        $data["page"] = "page404";
                    } else {
                        $data["page"] = "product";
                        $data["product_id"] = $product->getProductId();
                        $data["breadcrumbs_url"][] = $url_add;
                        $data["breadcrumbs_title"][] = $product->getTitle();
                        $data["product"] = $product;
                    }
                }
            }
        }
        return $data;
    }

    function updateCategory($lang, $data) {
        if (($lang == 'en') or ( $lang == 'cz') or ( $lang == 'de')) {
            if ($lang == 'de') {
                $lang = 'cz';
            }
            $db = Zend_Db_Table::getDefaultAdapter();
            $sql = "UPDATE categories SET
					category_url_$lang = \"" . $data['category_url'] . "\",
                    category_name_$lang = \"" . $data['category_name'] . "\",
					category_full_title_$lang = \"" . $data['category_full_title'] . "\",
                    category_sequence = '" . $data['category_sequence'] . "',
					category_maintainance_type = '" . $data['category_maintainance_type'] . "',
                    category_maintainance_$lang = \"" . trim($data['category_maintainance']) . "\",
					category_softness = '" . $data['category_softness'] . "'
                    WHERE category_id = '" . $data['category_id'] . "'
                    LIMIT 1";
            $stmt = $db->query($sql);
        } else {
            echo "Error: No category ID set!";
        }
    }

}

?>
