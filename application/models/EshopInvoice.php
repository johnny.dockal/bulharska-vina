<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of categories
 *
 * @author owner
 */
class Model_EshopInvoice extends Zend_Db_Table_Abstract {
    /**
     * Dopln� ��slo faktury o pot�ebn� po�et nul, aby m�la v�dy stejn� celkov� po�et ��slic.
     * @param int $cisloFaktury P�vodn� ��slo faktury.
     * @return int Zm�n�n� ��slo faktury.
     */
    public function getInvoiceNumber($orderId) {
        // dopln�n� nul p�ed ��slo faktury aby m�la v�dy stejn� po�et ��slic
        if ($orderId < 10) {
            $orderId = '0000' . $orderId;
        } else if ($orderId < 100) {
            $orderId = '000' . $orderId;
        } else if ($orderId < 1000) {
            $orderId = '00' . $orderId;
        } else if ($orderId < 10000) {
            $orderId = '0' . $orderId;
        }
        return $orderId;
    }
    
    public function getAccountNumber($id) {
        if ($id == 7) {
            $modelSettings  = new Settings();
            $number         = $modelSettings->fetchRow("setting_id = 'account_cz'");
            return $number->setting_value1;
        } else if ($id == 9) {
            $modelSettings  = new Settings();
            $number         = $modelSettings->fetchRow("setting_id = 'account_sk'");
            return $number->setting_value1;
        } else if ($id == 10) {
            $modelSettings  = new Settings();
            $number         = $modelSettings->fetchRow("setting_id = 'account_sipo'");
            return $number->setting_value1;  
        } else {
            return null;
        }
    }
    
    public function getBankId($id) {
        if ($id == 7) {
            $modelSettings  = new Settings();
            $number         = $modelSettings->fetchRow("setting_id = 'account_cz'");
            return $number->setting_value2;
        } else if ($id == 9) {
            $modelSettings  = new Settings();
            $number         = $modelSettings->fetchRow("setting_id = 'account_sk'");
            return $number->setting_value2;
        } else if ($id == 10) {
            $modelSettings  = new Settings();
            $number         = $modelSettings->fetchRow("setting_id = 'account_sipo'");
            return $number->setting_value2;  
        } else {
            return null;
        }
    }
 }
?>
