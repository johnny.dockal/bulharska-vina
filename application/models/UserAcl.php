<?php

/**
 * Vytvoření uživatelských rolí guests a admin.
 * Nastavení jejich oprávnění, tj. na jaké stránky mají přístup.
 * @author Daniel Vála
 */
class Model_UserAcl extends Zend_Acl {

    function __construct() {
        // Roles
        $this->addRole(new Zend_Acl_Role('guest'));
        $this->addRole(new Zend_Acl_Role('editor'), 'guest');
        $this->addRole(new Zend_Acl_Role('admin'), 'editor');
        $this->addRole(new Zend_Acl_Role('superadmin'), 'admin');
        
        // Resources
        // Default module
        $this->add(new Zend_Acl_Resource('default'))     
                ->add(new Zend_Acl_Resource('default:ajax', 'default'))           
                ->add(new Zend_Acl_Resource('default:index', 'default'))
                ->add(new Zend_Acl_Resource('default:novinky', 'default'))
                ->add(new Zend_Acl_Resource('default:cart', 'default'))
                ->add(new Zend_Acl_Resource('default:info', 'default'))
                ->add(new Zend_Acl_Resource('default:mailinglist', 'default'))
                ->add(new Zend_Acl_Resource('default:kontakt', 'default'))
                ->add(new Zend_Acl_Resource('default:sitemap', 'default'))
                ->add(new Zend_Acl_Resource('default:error', 'default'));       
        // Admin module
        
        // Permission
        // Guests mohou jen na defaultní modul.     
        $this->allow('guest', 'default:ajax');   
        $this->allow('guest', 'default:index');    
        $this->allow('guest', 'default:novinky');   
        $this->allow('guest', 'default:cart');   
        $this->allow('guest', 'default:info'); 
        $this->allow('guest', 'default:mailinglist');
        $this->allow('guest', 'default:kontakt');
        $this->allow('guest', 'default:sitemap');
        $this->allow('guest', 'default:error');

       
        
    }

}
