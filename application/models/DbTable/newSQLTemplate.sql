SELECT 
GROUP_CONCAT(c.category_id SEPARATOR ';') AS category_id, 
GROUP_CONCAT(c.eshop_id SEPARATOR ';') AS eshop_id, 
GROUP_CONCAT(s.subcategory_id SEPARATOR ';') AS subcategory_id, 
GROUP_CONCAT(s.title_cz SEPARATOR ';') AS subcat_title, 
GROUP_CONCAT(s.url_cz SEPARATOR ';') AS subcat_alias, p.product_id, p.status, p.code, p.sequence, p.title_cz AS title, p.alias_cz AS alias, p.full_title_cz AS full_title, p.text_cz AS text, p.note_cz AS note, p.material_cz AS material, p.manufactured_cz AS manufactured, p.price_unit_cz, p.price_unit_de, p.price_cz, p.price_de, p.size, p.weight, p.weight_unit, p.grams, p.hair, p.certified, p.new 
FROM eshop_products AS p 
JOIN (eshop_subcat_products AS sp1, eshop_subcategories AS s1, eshop_categories AS c1) 
    ON (sp1.product_id = p.product_id AND s1.subcategory_id = sp1.subcategory_id AND c1.category_id = s1.category_id)
LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s, eshop_categories AS c) 
    ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id AND c.category_id = s.category_id)
WHERE c1.category_id=1
GROUP BY p.product_id

KAMIL
SELECT 
GROUP_CONCAT(c.category_id SEPARATOR ';') AS category_id, 
GROUP_CONCAT(c.eshop_id SEPARATOR ';') AS eshop_id, 
GROUP_CONCAT(s.subcategory_id SEPARATOR ';') AS subcategory_id, 
GROUP_CONCAT(s.title_cz SEPARATOR ';') AS subcat_title, 
GROUP_CONCAT(s.url_cz SEPARATOR ';') AS subcat_alias, p.product_id, p.status, p.code, p.sequence, p.title_cz AS title, p.alias_cz AS alias, p.full_title_cz AS full_title, p.text_cz AS text, p.note_cz AS note, p.material_cz AS material, p.manufactured_cz AS manufactured, p.price_unit_cz, p.price_unit_de, p.price_cz, p.price_de, p.size, p.weight, p.weight_unit, p.grams, p.hair, p.certified, p.new 
FROM eshop_products AS p 
JOIN (eshop_subcat_products AS sp1, eshop_subcategories AS s1) ON (sp1.product_id = p.product_id AND s1.subcategory_id = sp1.subcategory_id )
JOIN (eshop_categories AS c1) ON (s1.category_id = c1.category_id)
LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s) ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id )
LEFT JOIN (eshop_categories AS c) ON (s.category_id = c.category_id)
where c1.category_id=1
GROUP BY p.product_id




SELECT     ep.product_id,    
                                    GROUP_CONCAT(es.category_id SEPARATOR ';') AS category_id,
                                    GROUP_CONCAT(es.subcategory_id SEPARATOR ';') AS subcategory_id,
                                    GROUP_CONCAT(es.title_cz SEPARATOR ';') AS subcategory_title,
                                    ep.status_$this->locale AS status,
                                    ep.vat_rate, 
                                    es.type AS type,
                                    ep.title_cz AS title,
                                    ep.text_cz AS text,
                                    ep.producer,
                                    ep.size,                                    
                                    ep.price_$this->locale AS price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text
                        FROM eshop_products AS ep JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id)








, eshop_categories AS c AND c.category_id = s.category_id AND c.category_id = 1 
select * from firmy f left join uz_firmy u on u.id = f.id and u.uid = 251
LEFT JOIN (eshop_categories AS c) ON (s.category_id = c.category_id AND c.category_id = 1)



SELECT 
GROUP_CONCAT(c.category_id SEPARATOR ';') AS category_id, 
GROUP_CONCAT(c.eshop_id SEPARATOR ';') AS esop_id, 
GROUP_CONCAT(c.title_cz SEPARATOR ';') AS category_title, 
GROUP_CONCAT(c.url_cz SEPARATOR ';') AS category_alias, 
GROUP_CONCAT(s.subcategory_id SEPARATOR ';') AS subcategory_id, 
GROUP_CONCAT(s.title_cz SEPARATOR ';') AS subcategory_title, 
GROUP_CONCAT(s.url_cz SEPARATOR ';') AS subcategory_alias, 
p.product_id, p.status, p.code, p.sequence, p.title_cz AS title, p.alias_cz AS alias, p.full_title_cz AS full_title, p.text_cz AS text, p.note_cz AS note, p.material_cz AS material, p.manufactured_cz AS manufactured, p.price_unit_cz AS price_unit, p.price_cz AS price, p.size, p.weight, p.weight_unit, p.grams, p.hair, p.certified, p.new, ph1.available FROM eshop_products AS p 
LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s, eshop_categories AS c) ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id AND c.category_id = s.category_id) 
LEFT JOIN (SELECT product_id, timestamp, (inv2b + inv3b) AS available FROM eshop_product_history AS ph1 WHERE timestamp = (SELECT MAX(timestamp) FROM eshop_product_history AS ph2 WHERE ph1.product_id = ph2.product_id)) AS ph1 ON p.product_id = ph1.product_id 
WHERE (c.eshop_id = 3) AND (status != '0') AND (status != '3') AND p.product_id IN (SELECT esp.product_id FROM eshop_subcat_products AS esp WHERE (esp.subcategory = 60) 
GROUP BY p.product_ID ORDER BY sequence, title