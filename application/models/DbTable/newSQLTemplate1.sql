SELECT DISTINCT 
esp.subcategory_id as category 
FROM eshop_subcat_products esp 
JOIN eshop_subcat_groups esg ON esp.subcategory_id = esg.subcategory_id 
WHERE esp.product_id IN ( 
    SELECT p.product_id 
    FROM eshop_subcat_groups AS esg 
    JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id 
    JOIN eshop_subcat_products AS esp ON esc.subcategory_id = esp.subcategory_id 
    JOIN eshop_products AS p ON esp.product_id = p.product_id 
    WHERE esc.subcategory_id = 60
    AND p.product_id IN (
        SELECT p.product_id 
        FROM eshop_subcat_groups AS esg 
        JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id 
        JOIN eshop_subcat_products AS esp ON esc.subcategory_id = esp.subcategory_id 
        JOIN eshop_products AS p ON esp.product_id = p.product_id 
        WHERE esp.subcategory_id = 87 
    )
) 
AND group_id = 3


SELECT 
GROUP_CONCAT(DISTINCT c.category_id SEPARATOR ';') AS category_id, 
GROUP_CONCAT(DISTINCT c.eshop_id SEPARATOR ';') AS esop_id, 
GROUP_CONCAT(DISTINCT c.title_cz SEPARATOR ';') AS category_title, 
GROUP_CONCAT(DISTINCT c.url_cz SEPARATOR ';') AS category_alias, 
GROUP_CONCAT(DISTINCT s.subcategory_id SEPARATOR ';') AS subcategory_id, 
GROUP_CONCAT(DISTINCT s.title_cz SEPARATOR ';') AS subcategory_title, 
GROUP_CONCAT(DISTINCT s.url_cz SEPARATOR ';') AS subcategory_alias, 
p.product_id, p.status, p.code, p.sequence, p.title_cz AS title, p.alias_cz AS alias, p.full_title_cz AS full_title, 
p.text_cz AS text, p.note_cz AS note, p.material_cz AS material, p.manufactured_cz AS manufactured, p.price_unit_cz AS price_unit, p.price_cz AS price, 
p.size, p.weight, p.weight_unit, p.grams, p.hair, p.certified, p.new 
FROM eshop_products AS p 
LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s, eshop_categories AS c) ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id AND c.category_id = s.category_id) 
WHERE (c.eshop_id = 3) AND (status != '0') AND (status != '3') AND p.price_cz BETWEEN '100' AND '750' AND p.product_id IN (SELECT esp.product_id FROM eshop_subcat_products AS esp WHERE esp.subcategory_id = '60') AND p.product_id IN (SELECT esp.product_id FROM eshop_subcat_products AS esp WHERE esp.subcategory_id = '85') AND p.product_id IN (SELECT esp.product_id FROM eshop_subcat_products AS esp WHERE esp.subcategory_id = '67') GROUP BY p.product_ID ORDER BY sequence, title
