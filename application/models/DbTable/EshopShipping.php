<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopShipping extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_deliveries';
    protected $_primary = 'shipping_id';

    /* pro pivovarský klub */

    public function fetchDeliveries() {
        $session = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array($this->_primary, 'public', 'title_' . $session->lang . ' AS title', 'text_' . $session->lang . ' AS text', 'price'));
        $result = $this->fetchAll($query)->toArray();
        return $result;
    }

    public function fetchShipping($country_id = null, $weight = null, $shipping_id = null) {
        $session = new Zend_Session_Namespace('Default');
        $db = Zend_Db_Table::getDefaultAdapter();
        if (isset($country_id) and isset($weight)) {
            if (isset($shipping_id)) {
                $sql = "SELECT
                        es.$this->_primary,
                        es.title_$session->lang AS title,
                        es.text_$session->lang AS text,

                        edps.id,
                        edps.price_set_id,
                        edps.weight_from,
                        edps.weight_till,
                        edps.price_cz AS price,

                        ecdp.id AS ecdp_id,
                        ecdp.country_id,
                        ecdp.public AS ecdp_public,
                        ecdp.public,
                        ecdp.payment_id
                        FROM eshop_deliveries AS es
                        JOIN eshop_deliveries_prices_sets AS edps
                        ON es.$this->_primary = edps.shipping_id
                        JOIN eshop_country_delivery_payment AS ecdp
                        ON edps.price_set_id = ecdp.price_set_id
                        WHERE ecdp.country_id = '$country_id'
                        AND edps.weight_from <= '$weight'
                        AND edps.weight_till > '$weight'
                        AND es.$this->_primary = '$shipping_id'";
            } else {
                $sql = "SELECT
                            es.shipping_id,
                            es.title_$session->lang AS title,
                            es.text_$session->lang AS text,

                            ecdp.id AS ecdp_id,
                            ecdp.price_set_id,
                            ecdp.country_id,
                            ecdp.public,
                            ecdp.payment_id 
                            FROM eshop_country_delivery_payment AS ecdp
                            JOIN eshop_deliveries AS es
                            ON es.shipping_id = ecdp.shipping_id

                            WHERE ecdp.country_id = '$country_id' "
                        . "AND ecdp.public = '1' "
                        . "GROUP BY es.shipping_id ";
            }
            try {
                if (isset($shipping_id)) {
                    $result = $db->fetchRow($sql);
                } else {
                    $result = $db->fetchAll($sql);
                    foreach ($result as $key => $value) {
                        $sql2 = "SELECT
                                    edps.id,
                                    edps.shipping_id,
                                    edps.price_set_id,
                                    edps.weight_from,
                                    edps.weight_till,
                                    edps.price_cz AS price
                                    FROM eshop_deliveries_prices_sets AS edps
                                    WHERE edps.weight_from <= '$weight' "
                                . "AND edps.weight_till > '$weight' "
                                . "AND edps.price_set_id = '" . $value['price_set_id'] . "' "
                                . "AND edps.shipping_id = '" . $value['shipping_id'] . "' ";
                        try {
                            $result2 = $db->fetchRow($sql2);
                        } catch (Zend_Exception $e) {
                            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                            echo "\n <br/>SQL: " . $sql2 . "\n <br/>";
                        }
                        $result[$key]['price'] = $result2['price'];
                    }
                }
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                echo "\n <br/>SQL: " . $sql . "\n <br/>";
            }
        } else {
            $sql = "SELECT 
                        $this->_primary,             
                        public, 
                        title_$session->lang AS title,      
                        text_$session->lang AS text                             
                    FROM $this->_name ";
            if (isset($shipping_id)) {
                $sql .= "WHERE shipping_id = '$shipping_id'";
            }
            try {
                if (isset($shipping_id)) {
                    $result = $db->fetchRow($sql);
                } else {
                    $result = $db->fetchAll($sql);
                }
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                echo "\n <br/>SQL: " . $sql . "\n <br/>";
            }
        }/*
          echo "<pre>";
          var_dump($result);
          echo "</pre>"; */
        return $result;
    }

}
