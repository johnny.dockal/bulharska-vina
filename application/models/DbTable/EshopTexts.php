<?php

/**
 * Model tabulky article.
 *
 * @package default
 * @author Daniel Vála
 */
class Model_DbTable_EshopTexts extends Model_DbTable_Texts {

    protected $_name = 'eshop_texts';
    protected $_primary = 'text_id';

    /**
     * Vybere z databáze všechny texty na stránky.
     * Předpokládá se, že text_id článku bude pojmenovávat typ článku.
     * Text_id se ve výsledném řetězci přehazuje jako klíč k poli $textarray, aby k tomu byl jednodužší přístup.
     * 
     * @return Zend_Db_Table_Rowset_Abstract
     */
    
    /* vrátí všechny texty v tabulce texts v daném jazyce 
     * 
     * @return array
     */
    /*
    public function fetchTexts() {
        $texts = $this->select();
        $defaultSession = new Zend_Session_Namespace('Default');
        //jazyk musí být dvoupísmenná zkratka, jestli tomu tak je se kontroluje už v pluginu LangSelector
        $texts->from('texts', array('text_id', 'title_'.$defaultSession->lang.' AS title', 'text_'.$defaultSession->lang.' AS text'));
        
        $result     = $this->fetchAll($texts);
        $textarray  = $result->toArray();
        for ($i = 0; $i < count($textarray); $i++) {
            $newkey             = $textarray[$i]['text_id'];
            $textarray[$newkey] = $textarray[$i];
            unset($textarray[$i]);
        }
        return $textarray;
    } */ 
    /* Vzhledem k tomu, že formuláře standartních textů jsou všechny stejné, 
     * můžeme si dovolit ukládat texty zde, abychom nemuseli sbírat parametry v každém kontrolleru zvlášť 
     * POZOR, tohle je nestandartní, normálně by se měli parametry předávat z controlleru, ale
     * tohle mi zabrání duplikaci kódu v kontrolerech. 
     * zároveň si tato funkce přečte, kolik je jazyků v configu, takže je použitelná všeobecně
     * pro libovolný počet jazyků v nastavení
     */
    /*
    public function updateText() {
        //abychom získali parametry z postu, musíme si zavolat singleton Froncontrolleru
        $fc                     = Zend_Controller_Front::getInstance();
        //zjistíme si dvoupísmenné jazyky nastavené v configu
        $config                 = Zend_Registry::get('config');        
        $langArray['enabled']   = explode(" ", $config->settings->languages->enabled);
        $text_id                = $fc->getRequest()->getParam('text_id');
        $data                   = array();
        foreach ($langArray['enabled'] as $lang) {
            $data['title_'.$lang]  = $fc->getRequest()->getParam('title_'.$lang);
            $data['text_'.$lang]   = $fc->getRequest()->getParam('text_'.$lang);
        }
        $where = $this->getAdapter()->quoteInto('text_id = ?', $text_id);
        $this->update($data, $where);
    }*/
    /*
    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }*/
}