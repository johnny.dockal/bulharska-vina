<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopOrders extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_orders';
    protected $_primary = 'order_id';

    public function fetchOrders($year = null, $filter = null, $eshop_id = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT "
                . "o.order_id, "
                . "o.eshop_id, "
                . "o.invoice_no, "
                . "o.status_id, "
                . "o.payment_id, "
                . "o.shipping_id, "
                . "o.payment_price, "
                . "o.shipping_price, "
                . "o.shipping_address, "
                . "o.user_id, "
                . "o.order_timestamp, "
                . "o.order_lang, "
                . "o.order_name, "
                . "o.order_surname, "
                . "o.order_phone, "
                . "o.order_email, "
                . "o.order_sendmail, "
                . "o.order_address, "
                . "o.order_city, "
                . "o.order_zip, "
                . "u.user_name, "
                . "s.title_cz AS title, "
                . "s.text_cz AS text, "
                . "s.color, "
                . "SUM(d.product_price * d.product_quantity) + o.payment_price + o.shipping_price AS order_price, "
                . "SUM(d.product_quantity) AS order_quantity "
                . "FROM $this->_name AS o "
                . "JOIN eshop_order_details AS d ON o.order_id = d.order_id "
                . "JOIN eshop_order_status AS s ON o.status_id = s.status_id "
                . "LEFT JOIN users AS u ON o.user_id = u.user_id ";
        if (isset($filter) && isset($year) && isset($eshop_id)) {
            $i = 0;
            $statuses = '';
            foreach ($filter as $value) {
                if (isset($value)) {
                    if ($i > 0) {
                        $statuses .= ", ";
                    }
                    $statuses .= "'$value'";
                    $i++;
                }
            }
            if ($year > 1) {
                $sql .= "WHERE YEAR(o.order_timestamp) = $year AND o.status_id IN ($statuses) ";
            } else {
                $sql .= "WHERE o.status_id IN ($statuses) ";
            }
            if (!empty($eshop_id)) {
                $sql .= "AND o.eshop_id = $eshop_id ";
            }    
        }
        $sql .= "GROUP BY o.order_id ORDER BY o.order_timestamp DESC";
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $value) {
                $value['order_price'] = $value['order_price'] + $value['payment_price'] + $value['shipping_price'];
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        
        return $result;
    }

    public function changeStatus($order_id, $status_id) {
        $user = Zend_Auth::getInstance()->getIdentity();
        $data = array('status_id' => $status_id, 'user_id' => $user->user_id);
        $where = $this->getAdapter()->quoteInto('order_id = ?', $order_id);
        try {
            if ($status_id >= 0) {
                $this->update($data, $where);
            }
            $model = new Model_DbTable_EshopOrderHistory();
            $data2 = array('order_id' => $order_id, 'status_id' => $status_id, 'user_id' => $user->user_id);
            $model->insert($data2);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }

    public function saveOrder($data) {
        $order_id = null;
        try {
            $this->insert($data);
            $db = Zend_Db_Table::getDefaultAdapter();
            $order_id = $db->lastInsertId($this->_name, $this->_primary);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $order_id;
    }

    public function getOrderByToken($token) {
        $sql = "SELECT * FROM $this->_name WHERE token = '$token'";
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }
    
    public function getPrevInvoiceNumber() {
        $sql = "SELECT MAX(invoice_no) FROM $this->_name";
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchRow($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result['MAX(invoice_no)'];
    }

}
