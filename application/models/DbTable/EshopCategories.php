<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopCategories extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_categories';
    protected $_primary = 'category_id';
    protected $lang = null;
    protected $columns = null;
    
    public function init() {
        $session = new Zend_Session_Namespace('Default');        
        //v německém locale je správná informace uložena v kolonce pro češtinu
        if ($session->lang == 'de') {
            $this->lang = 'cz';
        } else {
            $this->lang = $session->lang;
        }
        $this->columns = array(
            'category_id', 
            'sequence',
            'url_'.$this->lang.' AS url',
            'full_title_'.$this->lang.' AS full_title',
            'title_'.$this->lang.' AS title', 
            'text_'.$this->lang.' AS text',            
            'maintainance_'.$this->lang.' AS maintainance',
            'maintainance_type',
            'softness',
            'rowdisplay'
            );
    }
    
    public function fetchCategory($category_id) {
        $query = $this->select()->from($this->_name, $this->columns);        
        $query->where("category_id = '$category_id'");
        return $this->fetchRow($query)->toArray(); 
    } 
    
    public function fetchCategories($publicOnly = true, $shopSpecific = true) {
        $query = $this->select()->from($this->_name, $this->columns);        
        if ($publicOnly) {
            $query->where("public = '1'");
        }
        if ($shopSpecific) {
            $query->where("eshop_id = '".APP_ID."'");
        }          
        return $this->fetchAll($query)->toArray();
    }     
    
    public function fetchCategoryByAlias($alias) {  
        $query = $this->select()->from($this->_name, $this->columns);
        $query->where("url_$this->lang = '$alias'");
        $result = $this->fetchRow($query);
        return $result;        
    }

}