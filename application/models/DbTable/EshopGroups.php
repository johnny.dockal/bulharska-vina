<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopGroups extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_subcat_groups';
    protected $_primary = 'group_id';

    function fetchGroupedSubcategories($category_id) {
        $session = new Zend_Session_Namespace('Default');
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "SELECT eg.group_id, eg.category_id, es.subcategory_id, eg.title_$session->lang AS group_title, es.title_$session->lang AS subcat_title "
                . "FROM $this->_name AS eg "
                . "JOIN eshop_subcat_groups AS esg ON eg.group_id = esg.group_id "
                . "JOIN eshop_subcategories AS es ON esg.subcategory_id = es.subcategory_id "
                . "WHERE eg.category_id = '$category_id' "
                . "ORDER BY eg.group_id";
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

    function fetchGroupById($group_id, $only_occupied = false) {
        $session = new Zend_Session_Namespace('Default');
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT DISTINCT esg.group_id, esg.subcategory_id, esc.url_$session->lang AS alias, esc.title_$session->lang AS title "
                . "FROM $this->_name AS esg "
                . "JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id ";
        if ($only_occupied) {
            $sql .= "JOIN eshop_subcat_products AS esp ON esp.subcategory_id = esc.subcategory_id ";
                    //. "JOIN eshop_products AS ep ON ep.product_id = esp.product_id ";
        }
        $sql .= "WHERE esg.group_id = '$group_id' "
                //. "AND ep.status != '0' "
                . "ORDER BY esg.group_id, title ";
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }
    
    function fetchGroupByIdAjax($searched_group_id, $subcatId1 = 'default', $subcatId2 = 'default') {
        // $group_id   1 = typ vina  2 = odruda
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $cond = '';
        if($subcatId1 != 'default' AND $subcatId2 != 'default'){
            $cond = "WHERE esc.subcategory_id = ".$subcatId1." AND p.product_id IN ("
                    . "SELECT p.product_id "
                    . "FROM eshop_subcat_groups AS esg " 
                    . "JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id " 
                    . "JOIN eshop_subcat_products AS esp ON esc.subcategory_id = esp.subcategory_id " 
                    . "JOIN eshop_products AS p ON esp.product_id = p.product_id "
                    . "WHERE esp.subcategory_id = $subcatId2)";
        } elseif ($subcatId1 != 'default'){
            $cond = "WHERE esc.subcategory_id = ".$subcatId1."";
        } elseif ($subcatId2 != 'default'){
            $cond = "WHERE esc.subcategory_id = ".$subcatId2."";
        }
        
        $sql = "
        SELECT DISTINCT esp.subcategory_id as category
            FROM eshop_subcat_products esp 
            JOIN eshop_subcat_groups esg ON esp.subcategory_id = esg.subcategory_id
        WHERE esp.product_id IN (
            SELECT p.product_id
            FROM eshop_subcat_groups AS esg 
            JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id 
            JOIN eshop_subcat_products AS esp ON esc.subcategory_id = esp.subcategory_id 
            JOIN eshop_products AS p ON esp.product_id = p.product_id
            ".$cond."             
            AND p.status != 0    
        ) AND group_id = ".$searched_group_id."";
        
        try {
            //echo "<p>$sql</p>";
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }    
    /*
    function fetchGroupByIdAjax($searched_group_id, $subcatId1 = 'default', $subcatId2 = 'default') {
        // $group_id   1 = typ vina  2 = odruda
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $cond = '';
        if($subcatId1 != 'default' AND $subcatId2 != 'default'){
            $cond = "WHERE esc.subcategory_id = ".$subcatId1." AND esc.subcategory_id = ".$subcatId2."";
        }elseif($subcatId1 != 'default'){
            $cond = "WHERE esc.subcategory_id = ".$subcatId1."";
        }elseif($subcatId2 != 'default'){
            $cond = "WHERE esc.subcategory_id = ".$subcatId2."";
        }
        
        $sql = "
        SELECT DISTINCT esp.subcategory_id as category
            FROM eshop_subcat_products esp 
            JOIN eshop_subcat_groups esg ON esp.subcategory_id = esg.subcategory_id
        WHERE esp.product_id IN (
            SELECT p.product_id
            FROM eshop_subcat_groups AS esg 
            JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id 
            JOIN eshop_subcat_products AS esp ON esc.subcategory_id = esp.subcategory_id 
            JOIN eshop_products AS p ON esp.product_id = p.product_id
            ".$cond."
        ) AND group_id = ".$searched_group_id."";
        
        try {
            //echo "<p>$sql</p>";
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }*/

}
