<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopShops extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_shops';
    protected $_primary = 'eshop_id';
    protected $lang = null;
    
    public function init() {
        $session = new Zend_Session_Namespace('Default');        
        //v německém locale je správná informace uložena v kolonce pro češtinu
        if ($session->lang == 'de') {
            $this->lang = 'cz';
        } else {
            $this->lang = $session->lang;
        }
        $this->columns = array(
            'eshop_id',             
            'title_'.$this->lang.' AS title',            
        );
    }
    
    public function fetchEshops() {  
        $query = $this->select()->from($this->_name, array("eshop_id", "title_$this->lang AS title"));
        $result = $this->fetchAll($query);
        return $result;        
    }

}