<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Mailinglistform extends Zend_Form {

    public function __construct($action) {
        parent::__construct();
        $this->setMethod('POST')->setName('mailingform')->setAction($action);
        
        $address = new Zend_Form_Element_Text('address', array('placeholder' => 'Váš e-mail'));
        $address->setRequired(true);
        $address->removeDecorator('Label');
        
        $redirect = new Zend_Form_Element_Hidden('redirect');
        $redirect->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit('submit', array('id' => 'mailingformsubmit', 'label' => "OK", 'class' => 'submit click'));

        $this->addElements(array(
            $address, $redirect, $submit
        ));
    }
}
