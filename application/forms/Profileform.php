<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_PRofileform extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->setMethod('POST')->setName('adduser')->setAction('/admin/profile/save/');
        $this->setAttrib('class', 'admintable');

        $user_id = new Zend_Form_Element_Hidden('user_id');
        $user_id->setDecorators(array('ViewHelper'));
        
        $login = new Zend_Form_Element_Text('user_login', array("readonly" => "readonly"));
        $login->setLabel('Uživatelský login:')->setRequired(true);

        $password = new Zend_Form_Element_Password('user_pswd');
        $password->setLabel('Nové heslo:')->setRequired(true);
        
        $password2 = new Zend_Form_Element_Password('pswd_check');
        $password2->setLabel('Nové heslo (zopakovat):')->setRequired(true);
        
        $email = new Zend_Form_Element_Text('user_email');
        $email->setLabel('Email (bude se hodit pokud zapomeneš heslo):')->setRequired(true);
        
        $username = new Zend_Form_Element_Text('user_name');
        $username->setLabel('Celé jméno uživatele:')->setRequired(true);  
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => 'Uložit', 'class' => 'savebutton'));
        $submit->setIgnore(true);

        $this->addElements(array(
            $user_id, $login, $password, $password2, $email, $username, $submit
        ));
    }

}
