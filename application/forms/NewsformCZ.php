<?php
class Form_NewsformCZ extends Zend_Form {

    public function __construct($action) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        
        //momentálně je formulář nastaven na dva jazyky (cz a en), nevím jak ho upravit pro libovolný počet jazyků z configu
        $news_id = new Zend_Form_Element_Hidden('news_id');
        //zabání zobrazení labelu
        $news_id->removeDecorator('label');
        
        $date = new Zend_Dojo_Form_Element_TextBox('date');
        $date->setLabel('Datum (YYYY-MM-DD):')->setRequired(true);
        
        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide'));
        $title_cz->setLabel('Nadpis česky (nepoviiný):');

        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxhuge"));
        $text_cz->setLabel('Text česky:')->setRequired(true);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));

        $this->addElements(array(
            $news_id, $date, $title_cz, $text_cz, $submit
        ));
    }
}
