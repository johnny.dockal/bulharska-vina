<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_ProductsformCZENDE extends Zend_Form {

    public function __construct($action = null, $options = null, $selected = null, $id = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('enctype', 'multipart/form-data');

        $product_id = new Zend_Form_Element_Hidden('product_id');
        $product_id->removeDecorator('Label');

        $subcategory_id = new Zend_Form_Element_MultiCheckbox('subcategory_id');
        $subcategory_id->setLabel('Subkategorie:')->setRequired(true);
        if (isset($options)) {
            foreach ($options as $value) {
                $subcategory_id->addMultiOption($value['subcategory_id'], "[" . $value['locale'] . "] " . $value['cat_title'] . " - " . $value['subcat_title']);
            }
            $subcategory_id->setValue($selected);
        }

        $save = new Zend_Form_Element_Submit('save', array('label' => "Uložit", 'class' => "button bg-green border-style active", 'style' => 'width: 99%;'));

        $status_cz = new Zend_Form_Element_Select('status_cz', array('class' => 'textboxwide'));
        $status_cz->addMultiOptions(array('1' => 'prodaván na eshopu', '2' => 'vystaven na eshopu (nelze dát do košíku)', '0' => 'neaktivní', '3' => 'pouze v obchodě'));
        $status_cz->setLabel('Status výrobku na ČESKÝCH stránkách:')->setRequired(true);
        
        $status_de = new Zend_Form_Element_Select('status_de', array('class' => 'textboxwide'));
        $status_de->addMultiOptions(array('1' => 'prodaván na eshopu', '2' => 'vystaven na eshopu (nelze dát do košíku)', '0' => 'neaktivní', '3' => 'pouze v obchodě'));
        $status_de->setLabel('Status výrobku na NĚMECKÝCH stánkách:')->setRequired(true);

        $code = new Zend_Dojo_Form_Element_TextBox('code', array('class' => 'textboxwide'));
        $code->setLabel('Kód produktu:')->setRequired(true);

        $sequence = new Zend_Dojo_Form_Element_TextBox('sequence', array('class' => 'textboxwide'));
        $sequence->setLabel('Sekvence:')->setRequired(true);

        //ceny v různém jazyce
        $price_unit_cz = new Zend_Dojo_Form_Element_TextBox('price_unit_cz', array('class' => 'textboxwide'));
        $price_unit_cz->setLabel('Cena za metr2 / běžný metr kč (nikoliv cena produktu):')->setRequired(true);
        $price_unit_de = new Zend_Dojo_Form_Element_TextBox('price_unit_de', array('class' => 'textboxwide'));
        $price_unit_de->setLabel('Cena za metr2 / běžný metr € (nikoliv cena produktu):')->setRequired(true);
        $price_cz = new Zend_Dojo_Form_Element_TextBox('price_cz', array('class' => 'textboxwide'));
        $price_cz->setLabel('Cena produktu kč:')->setRequired(true);
        $price_de = new Zend_Dojo_Form_Element_TextBox('price_de', array('class' => 'textboxwide'));
        $price_de->setLabel('Cena produktu €:')->setRequired(true);

        $this->addElements(array(
            $product_id,
            $status_cz,
            $status_de,
            $subcategory_id,
            $code,
            $sequence,
            $price_unit_cz,
            $price_unit_de,
            $price_cz,
            $price_de,
            $save
        ));

        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide'));
        $title_cz->setLabel('Nadpis česky:')->setRequired(true);
        $full_title_cz = new Zend_Dojo_Form_Element_TextBox('full_title_cz', array('class' => 'textboxwide'));
        $full_title_cz->setLabel('Rozšířený nadpis česky (např. Vlněná deka Rodopa):')->setRequired(true);
        $alias_cz = new Zend_Dojo_Form_Element_TextBox('alias_cz', array('class' => 'textboxwide'));
        $alias_cz->setLabel('URL alias cz:')->setRequired(true);
        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxbig"));
        $text_cz->setLabel('Text česky:')->setRequired(true);
        $note_cz = new Zend_Dojo_Form_Element_TextBox('note_cz', array('class' => 'textboxwide'));
        $note_cz->setLabel('Poznámka česky:')->setRequired(true);
        $material_cz = new Zend_Dojo_Form_Element_TextBox('material_cz', array('class' => 'textboxwide'));
        $material_cz->setLabel('Materiál česky:')->setRequired(true);
        $manufactured_cz = new Zend_Dojo_Form_Element_TextBox('manufactured_cz', array('class' => 'textboxwide'));
        $manufactured_cz->setLabel('Vyrobeno česky:')->setRequired(true);

        $this->addElements(array(
            $title_cz,
            $full_title_cz,
            $alias_cz,
            $note_cz,
            $material_cz,
            $manufactured_cz,
            $text_cz
        ));

        $this->addDisplayGroup(array(
            'title_cz', 'alias_cz', 'full_title_cz', 'note_cz', 'material_cz', 'manufactured_cz', 'text_cz'
                ), 'group_cz', array('legend' => 'Čeština'));
        $group_cz = $this->getDisplayGroup('group_cz');
        $group_cz->setDecorators(array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div'))
        ));

        $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textboxwide'));
        $title_en->setLabel('Nadpis anglicky:')->setRequired(true);
        $full_title_en = new Zend_Dojo_Form_Element_TextBox('full_title_en', array('class' => 'textboxwide'));
        $full_title_en->setLabel('Rozšířený nadpis anglicky (např. Vlněná deka Rodopa):')->setRequired(true);
        $alias_en = new Zend_Dojo_Form_Element_TextBox('alias_en', array('class' => 'textboxwide'));
        $alias_en->setLabel('URL alias en:')->setRequired(true);
        $text_en = new Zend_Dojo_Form_Element_Textarea('text_en', array('class' => "textboxbig"));
        $text_en->setLabel('Text anglicky:')->setRequired(true);
        $note_en = new Zend_Dojo_Form_Element_TextBox('note_en', array('class' => 'textboxwide'));
        $note_en->setLabel('Poznámka anglicky:')->setRequired(true);
        $material_en = new Zend_Dojo_Form_Element_TextBox('material_en', array('class' => 'textboxwide'));
        $material_en->setLabel('Materiál anglicky:')->setRequired(true);
        $manufactured_en = new Zend_Dojo_Form_Element_TextBox('manufactured_en', array('class' => 'textboxwide'));
        $manufactured_en->setLabel('Vyrobeno anglicky:')->setRequired(true);

        $this->addElements(array(
            $title_en,
            $full_title_en,
            $alias_en,
            $note_en,
            $material_en,
            $manufactured_en,
            $text_en
        ));

        $this->addDisplayGroup(array(
            'title_en', 'alias_en', 'full_title_en', 'note_en', 'material_en', 'manufactured_en', 'text_en'
                ), 'group_en', array('legend' => 'Angličtina'));
        $group_en = $this->getDisplayGroup('group_en');
        $group_en->setDecorators(array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div'))
        ));

        $title_de = new Zend_Dojo_Form_Element_TextBox('title_de', array('class' => 'textboxwide'));
        $title_de->setLabel('Nadpis německy:')->setRequired(true);
        $full_title_de = new Zend_Dojo_Form_Element_TextBox('full_title_de', array('class' => 'textboxwide'));
        $full_title_de->setLabel('Rozšířený nadpis německy (např. Vlněná deka Rodopa):')->setRequired(true);
        $alias_de = new Zend_Dojo_Form_Element_TextBox('alias_de', array('class' => 'textboxwide'));
        $alias_de->setLabel('URL alias de:')->setRequired(true);
        $text_de = new Zend_Dojo_Form_Element_Textarea('text_de', array('class' => "textboxbig"));
        $text_de->setLabel('Text německy:')->setRequired(true);
        $note_de = new Zend_Dojo_Form_Element_TextBox('note_de', array('class' => 'textboxwide'));
        $note_de->setLabel('Poznámka německy:')->setRequired(true);
        $material_de = new Zend_Dojo_Form_Element_TextBox('material_de', array('class' => 'textboxwide'));
        $material_de->setLabel('Materiál německy:')->setRequired(true);
        $manufactured_de = new Zend_Dojo_Form_Element_TextBox('manufactured_de', array('class' => 'textboxwide'));
        $manufactured_de->setLabel('Vyrobeno německy:')->setRequired(true);

        $this->addElements(array(
            $title_de,
            $full_title_de,
            $alias_de,
            $note_de,
            $material_de,
            $manufactured_de,
            $text_de
        ));

        $this->addDisplayGroup(array(
            'title_de', 'alias_de', 'full_title_de', 'note_de', 'material_de', 'manufactured_de', 'text_de'
                ), 'group_de', array('legend' => 'Němčina'));
        $group_de = $this->getDisplayGroup('group_de');
        $group_de->setDecorators(array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div'))
        ));

        $size = new Zend_Dojo_Form_Element_TextBox('size', array('class' => 'textboxwide'));
        $size->setLabel('Velikost:')->setRequired(true);

        $weight = new Zend_Dojo_Form_Element_TextBox('weight', array('class' => 'textboxwide'));
        $weight->setLabel('Váha:')->setRequired(true);

        $weight_unit = new Zend_Dojo_Form_Element_TextBox('weight_unit', array('class' => 'textboxwide'));
        $weight_unit->setLabel('Gramáž g/m2 (pouze číslo):')->setRequired(true);

        $grams = new Zend_Dojo_Form_Element_TextBox('grams', array('class' => 'textboxwide'));
        $grams->setLabel('Gramy:')->setRequired(true);

        $hair = new Zend_Dojo_Form_Element_TextBox('hair', array('class' => 'textboxwide'));
        $hair->setLabel('Délka vlasu:')->setRequired(true);

        $certified = new Zend_Dojo_Form_Element_CheckBox('certified', array('class' => 'textboxwide'));
        $certified->setLabel('Certifikovaný?:')->setRequired(true);

        $new = new Zend_Dojo_Form_Element_CheckBox('new', array('class' => 'textboxwide'));
        $new->setLabel('Nový?:')->setRequired(true);

        $image1 = new Zend_Form_Element_File('product_image1');
        $image1->setLabel('Hlavní obrázek produktu (velikost 200pixelů na šířku, ideálně čtvercový, ale může být i delší):');
        $image1->addValidator('Count', false, 1);
        //$image->addValidator('Size', false, 10240000);
        $image1->addValidator('Extension', false, 'jpg,JPG');
        $image1->addDecorators(array(
                array(array("img" => "HtmlTag"), array(
                        "tag" => "img",
                        "openOnly" => true,
                        "src" => "/images/eshop_products/$id.jpg",
                        "align" => "middle",
                        "class" => ""
                    )),
                array(array("span" => "HtmlTag"), array(
                        "tag" => "span",
                        "class" => "myElement"
                    ))
        ));

        $image2 = new Zend_Form_Element_File('product_image2');
        $image2->setLabel('Velký obrázek produktu (velikost 400 pixelů na šířku):');
        $image2->addValidator('Count', false, 1);
        //$image->addValidator('Size', false, 10240000);
        $image2->addValidator('Extension', false, 'jpg,JPG');
        $image2->addDecorators(array(
                array(array("img" => "HtmlTag"), array(
                        "tag" => "img",
                        "openOnly" => true,
                        "src" => "/images/eshop_products/".$id."_big.jpg",
                        "align" => "middle",
                        "class" => ""
                    )),
                array(array("span" => "HtmlTag"), array(
                        "tag" => "span",
                        "class" => "myElement"
                    ))
        ));

        $image3 = new Zend_Form_Element_File('product_image3');
        $image3->setLabel('Detail produktu (velikost jakákoliv, ne více než 800 pixelů na šířku):');
        $image3->addValidator('Count', false, 1);
        //$image->addValidator('Size', false, 10240000);
        $image3->addValidator('Extension', false, 'jpg,JPG');
        $image3->addDecorators(array(
                array(array("img" => "HtmlTag"), array(
                        "tag" => "img",
                        "openOnly" => true,
                        "src" => "/images/eshop_products/".$id."_detail.jpg",
                        "align" => "middle",
                        "class" => ""
                    )),
                array(array("span" => "HtmlTag"), array(
                        "tag" => "span",
                        "class" => "myElement"
                    ))
        ));

        $save2 = new Zend_Form_Element_Submit('save2', array('label' => "Uložit", 'class' => "button bg-green border-style active", 'style' => 'width: 99%;'));

        $this->addElements(array(
            $size,
            $weight,
            $weight_unit,
            $grams,
            $hair,
            $certified,
            $new,
            $image1,
            $image2,
            $image3,
            $save2
        ));
    }

}
