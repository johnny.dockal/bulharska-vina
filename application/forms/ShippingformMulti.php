<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Shippingform extends Zend_Form {

    public function __construct($shipping = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/admin/shipping/saveshipping/');
        $this->setAttrib('class', 'admintable');
        
        if (!isset($shipping)) {
            $shipping = array('0');
            $i = 0;
        } else {
            $i = 1;
        }
        foreach ($shipping as $value) {
            $setting_id = new Zend_Form_Element_Hidden($i.'shipping_id');
            $setting_id->setDecorators(array('ViewHelper'));
            $public = new Zend_Form_Element_Radio($i.'public');
            $public->addMultiOptions(array(
                           '0'    => 'Zákázáno',
                           '1'    => 'Umožněno', 
                ));  
            $public->setValue('0');
            $public->setLabel('Status:')->setRequired(true);
            $title_cz = new Zend_Dojo_Form_Element_TextBox($i.'title_cz', array('class' => 'textbox'));
            $title_cz->setLabel('Název česky')->setRequired(true);
            $text_cz = new Zend_Dojo_Form_Element_Textarea($i.'text_cz', array('class' => 'textbox'));
            $text_cz->setLabel('Doprovodný text česky')->setRequired(true);
            $title_en = new Zend_Dojo_Form_Element_TextBox($i.'title_en', array('class' => 'textbox'));
            $title_en->setLabel('Název anglicky')->setRequired(true);
            $text_en = new Zend_Dojo_Form_Element_Textarea($i.'text_en', array('class' => 'textbox'));
            $text_en->setLabel('Doprovodný text anglicky')->setRequired(true);
            $title_de = new Zend_Dojo_Form_Element_TextBox($i.'title_de', array('class' => 'textbox'));
            $title_de->setLabel('Název německy')->setRequired(true); 
            $text_de = new Zend_Dojo_Form_Element_Textarea($i.'text_de', array('class' => 'textbox'));
            $text_de->setLabel('Doprovodný text německy')->setRequired(true); 
            $this->addElements(array($setting_id, $public, $title_cz, $text_cz, $title_en, $text_en, $title_de, $text_de));
            if (empty($value['title_cz'])) {
                $title = 'Nový záznam';
            } else {
                $title = $value['title_cz'];
            }
            $this->addDisplayGroup(array(        
                    $i.'public',
                    $i.'title_cz',
                    $i.'text_cz',
                    $i.'title_en',
                    $i.'text_en',
                    $i.'title_de',
                    $i.'text_de'
            ),$i.'group',array('legend' => $title));
            $group = $this->getDisplayGroup($i.'group');
            $group->setDecorators(array(        
                    'FormElements',
                    'Fieldset',
                    array('HtmlTag',array('tag'=>'div'))
            ));            
            $i++;
        }
        
        $count = new Zend_Form_Element_Hidden('count');
        $count->setValue($i);
        $count->removeDecorator('label');
        $this->addElement($count);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "button bg-green border-style active"));
        $this->addElement($submit);
        
    }

}
