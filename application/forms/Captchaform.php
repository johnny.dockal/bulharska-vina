<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Captchaform extends Zend_Form {

    public function __construct($action) {
        parent::__construct();                
        $this->setMethod('POST')->setName('captchaform');
        $captha = new Zend_Form_Element_Captcha('captcha', array(
            'label' => "Děkujeme!",
            'captcha' => 'Dumb',
            'captchaOptions' => array(
                'captcha' => 'Dumb',
                'wordLen' => 4,
                'timeout' => 300,
            ),
        ));
        $captha->setDecorators(array('Label', 'ViewHelper'));
        
        $email = new Zend_Form_Element_Hidden('captchaaddress', array('value' => '', 'class' => "floatleft"));
        $email->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit('captchasubmit', array('label' => 'OK'));
        $submit->setDecorators(array('ViewHelper'));
        
        $this->addElements(array(
            $captha, $submit, $email
        ));
    }

}
