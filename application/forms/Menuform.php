<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Menuform extends Zend_Form {

    public function __construct($action, $data = null, $date = null, $akce = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        $template = 
"<table border='0' cellspacing='0' cellpadding='0' width='582'>
<colgroup><col width='56'></col> <col width='53'></col> <col width='57'></col> <col width='54'></col> <col width='309'></col> <col width='53'></col> </colgroup> 
<tbody>
<tr height='27'>
<td class='xl25' colspan='5' width='529' height='27'></td>
<td class='xl24' width='53'></td>
</tr>
<tr height='27'>
<td class='xl25' colspan='5' height='27'></td>
<td class='xl24'></td>
</tr>
<tr height='27'>
<td class='xl25' colspan='5' height='27'></td>
<td class='xl24'></td>
</tr>
<tr height='27'>
<td class='xl25' colspan='5' height='27'></td>
<td class='xl24'></td>
</tr>
<tr height='27'>
<td class='xl25' colspan='5' height='27'></td>
<td class='xl24'></td>
</tr>
<tr height='27'>
<td class='xl25' colspan='5' height='27'></td>
<td class='xl24'></td>
</tr>
</tbody>
</table>";
$template = "";        
        //pokud není dáno datum, tak se předpokládá momentální týden
        if (!isset($date)) {
            $month  = date('n');
            $day    = date('d');
            $year   = date('Y');  
        } else { 
            $year   = substr($date, 0, 4);
            $day    = substr($date, -2, 2);
            $month  = substr($date, 5, 2);
        }
        if (empty($data)) {
            $w = date('w');
            //neděle je standartně nula, to se mi nelíbí
            if ($w == 0) {
                $w = 7;
            }
            $w--;
            $monday     = date('Y-m-d', mktime(0,0,0,$month,$day-$w,$year));
            $w--;
            $tuesday    = date('Y-m-d', mktime(0,0,0,$month,$day-$w,$year));
            $w--;
            $wednesday  = date('Y-m-d', mktime(0,0,0,$month,$day-$w,$year));
            $w--;
            $thursday   = date('Y-m-d', mktime(0,0,0,$month,$day-$w,$year));
            $w--;
            $friday     = date('Y-m-d', mktime(0,0,0,$month,$day-$w,$year));
            /*
            $w--;
            $saturday   = date('Y-d-m', mktime(0,0,0,$month,$day-$w,$year));
            $w--;
            $sunday     = date('Y-d-m', mktime(0,0,0,$month,$day-$w,$year)); */ 
        } 
        
        $textbox  = new Zend_Dojo_Form_Element_Textarea('akce', array('class' => 'textboxwide', 'value' => (isset($akce)) ? $akce : ""));
        $textbox ->setLabel('Doplňující zpráva / akce:')->setRequired(true);
        
        $key       = (isset($monday)) ? $monday : key($data);
        $value     = (!empty($data)) ? current($data) : $template;
        $monday_id = new Zend_Form_Element_Hidden('monday_id', array('class' => 'nodisplay', 'value' => $key));
        $monday_cz = new Zend_Dojo_Form_Element_Textarea('monday_cz', array('class' => 'textboxbig', 'value' => $value));    
        $monday_cz->setLabel('Pondělí '.$key.':')->setRequired(true);
        next($data);
        
        $key       = (isset($monday)) ? $tuesday : key($data);
        $value     = (!empty($data)) ? current($data) : $template;
        $tuesday_id = new Zend_Form_Element_Hidden('tuesday_id', array('class' => 'nodisplay', 'value' => $key));
        $tuesday_cz = new Zend_Dojo_Form_Element_Textarea('tuesday_cz', array('class' => 'textboxbig', 'value' => $value));
        $tuesday_cz->setLabel('Úterý '.$key.':')->setRequired(true);
        next($data);
        
        $key       = (isset($monday)) ? $wednesday : key($data);
        $value     = (!empty($data)) ? current($data) : $template;
        $wednesday_id = new Zend_Form_Element_Hidden('wednesday_id', array('class' => 'nodisplay', 'value' => $key));
        $wednesday_cz = new Zend_Dojo_Form_Element_Textarea('wednesday_cz', array('class' => 'textboxbig', 'value' => $value));
        $wednesday_cz->setLabel('Středa '.$key.':')->setRequired(true);
        next($data);
        
        $key       = (isset($monday)) ? $thursday : key($data);
        $value     = (!empty($data)) ? current($data) : $template;
        $thursday_id = new Zend_Form_Element_Hidden('thursday_id', array('class' => 'nodisplay', 'value' => $key));
        $thursday_cz = new Zend_Dojo_Form_Element_Textarea('thursday_cz', array('class' => 'textboxbig', 'value' => $value));
        $thursday_cz->setLabel('Čtrvrtek '.$key.'')->setRequired(true);
        next($data);
        
        $key       = (isset($monday)) ? $friday : key($data);
        $value     = (!empty($data)) ? current($data) : $template;
        $friday_id = new Zend_Form_Element_Hidden('friday_id', array('class' => 'nodisplay', 'value' => $key));
        $friday_cz = new Zend_Dojo_Form_Element_Textarea('friday_cz', array('class' => 'textboxbig', 'value' => $value));
        $friday_cz->setLabel('Pátek '.$key.':')->setRequired(true);        

        $date = new Zend_Form_Element_Hidden('date', array('class' => 'nodisplay', 'value' => $date));
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));

        $this->addElements(array(
            $textbox, $monday_id, $monday_cz, $tuesday_id, $tuesday_cz, $wednesday_id, $wednesday_cz, $thursday_id, $thursday_cz, $friday_id, $friday_cz, $date, $submit
        ));
    }
}
