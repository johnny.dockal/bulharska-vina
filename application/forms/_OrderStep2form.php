<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_OrderStep2form extends Zend_Form {

    public function __construct($deliveryOptions, $paymentOptions) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/eshop/cart/order/?step=3');
        $this->setAttrib('class', 'orderform');

        $delivery_options_formatted = array();
        foreach ($deliveryOptions as $value) {
            if ($value['public'] == 1) {
                $delivery_options_formatted[$value['delivery_id']] = "&nbsp;" . $value['title'] . " <p class='order-description'>" . $value['text'] . "</p><p class='order-description'>cena: <strong>+ " . $value['price'] . " kč</strong></p>";
            }
        }
        $delivery = new Zend_Form_Element_Radio('delivery_id', array(
                    'multiOptions' => $delivery_options_formatted,
                    'escape' => false,
                    'required' => true
                ));
        $delivery->removeDecorator('label');

        $payment_options_formatted = array();
        foreach ($paymentOptions as $value) {
            if ($value['public'] == 1) {
                $payment_options_formatted[$value['payment_id']] = "&nbsp;" . $value['title'] . " <p class='order-description'>" . $value['text'] . "</p><p class='order-description'>cena: <strong>+ " . $value['price'] . " kč</strong></p>";
            }
        }
        $payment = new Zend_Form_Element_Radio('payment_id', array(
                    'multiOptions' => $payment_options_formatted,
                    'escape' => false,
                    'required' => true
                ));
        $payment->removeDecorator('label');

        $this->addElements(array(
            $delivery, $payment
        ));

        $this->addDisplayGroup(array(
            'delivery_id',
                ), 'delivery', array('legend' => 'Způsob doručení:'));
        $delivery_group = $this->getDisplayGroup('delivery');
        $delivery_group->setDecorators(array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div'))
        ));
        $this->addDisplayGroup(array(
            'payment_id',
                ), 'payment', array('legend' => 'Způsob platby:'));

        $payment_group = $this->getDisplayGroup('payment');
        $payment_group->setDecorators(array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div'))
        ));
        
        $message = new Zend_Form_Element_Textarea('order_message', array('style' => 'height: 100px; width: 97%'));
        $message->removeDecorator('label');
        $message->setAttrib('maxlength','1000');
        $this->addElement($message);
        
        $this->addDisplayGroup(array(
            'order_message',
                ), 'message', array('legend' => 'Zpráva pro příjemce:'));

        $message_group = $this->getDisplayGroup('message');
        $message_group->setDecorators(array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div'))
        ));
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Pokračovat", 'class' => "button bg-green border-style active"));

        $this->addElement($submit);
    }

}
