<?php

class Form_Pivoform extends Zend_Form {
    
    public function __construct($action, $text = null) {
        parent::__construct($text);
        $this->setMethod('POST')->setName('pivo')->setAction($action);
        $this->setAttrib('class', 'admintable');
        
        $text_nacepu_cz = new Zend_Form_Element_Text('text_nacepu_cz', array('class' => 'textboxwide', 'value' => $text[0]['text_cz']));
        $text_nacepu_cz->setLabel('Na čepu (česky):')->setRequired(true);
        
        $text_nacepu_en = new Zend_Form_Element_Text('text_nacepu_en', array('class' => 'textboxwide', 'value' => $text[0]['text_en']));
        $text_nacepu_en->setLabel('Na čepu (anglicky):')->setRequired(true);
        
        $title_cz_pivo1 = new Zend_Form_Element_Text('title_cz_pivo1', array('class' => 'textboxwide', 'value' => $text[1]['title_cz']));
        $title_cz_pivo1->setLabel('Název pivo 1:')->setRequired(true);
        $text_cz_pivo1 = new Zend_Form_Element_Text('text_cz_pivo1', array('class' => 'textboxwide', 'value' => $text[1]['text_cz']));
        $text_cz_pivo1->setLabel('Popis pivo 1:')->setRequired(true);
        
        $title_cz_pivo2 = new Zend_Form_Element_Text('title_cz_pivo2', array('class' => 'textboxwide', 'value' => $text[2]['title_cz']));
        $title_cz_pivo2->setLabel('Název pivo 2:')->setRequired(true);
        $text_cz_pivo2 = new Zend_Form_Element_Text('text_cz_pivo2', array('class' => 'textboxwide', 'value' => $text[2]['text_cz']));
        $text_cz_pivo2->setLabel('Popis pivo 2:')->setRequired(true);
        
        $title_cz_pivo3 = new Zend_Form_Element_Text('title_cz_pivo3', array('class' => 'textboxwide', 'value' => $text[3]['title_cz']));
        $title_cz_pivo3->setLabel('Název pivo 3:')->setRequired(true);
        $text_cz_pivo3 = new Zend_Form_Element_Text('text_cz_pivo3', array('class' => 'textboxwide', 'value' => $text[3]['text_cz']));
        $text_cz_pivo3->setLabel('Popis pivo 3:')->setRequired(true);
        
        $title_cz_pivo4 = new Zend_Form_Element_Text('title_cz_pivo4', array('class' => 'textboxwide', 'value' => $text[4]['title_cz']));
        $title_cz_pivo4->setLabel('Naázev pivo 4:')->setRequired(true);
        $text_cz_pivo4 = new Zend_Form_Element_Text('text_cz_pivo4', array('class' => 'textboxwide', 'value' => $text[4]['text_cz']));
        $text_cz_pivo4->setLabel('Popis pivo 4:')->setRequired(true);
        
        $title_cz_pivo5 = new Zend_Form_Element_Text('title_cz_pivo5', array('class' => 'textboxwide', 'value' => $text[5]['title_cz']));
        $title_cz_pivo5->setLabel('Název pivo 5:')->setRequired(true);
        $text_cz_pivo5 = new Zend_Form_Element_Text('text_cz_pivo5', array('class' => 'textboxwide', 'value' => $text[5]['text_cz']));
        $text_cz_pivo5->setLabel('Popis pivo 5:')->setRequired(true);
        
        $title_cz_pivo6 = new Zend_Form_Element_Text('title_cz_pivo6', array('class' => 'textboxwide', 'value' => $text[6]['title_cz']));
        $title_cz_pivo6->setLabel('Název pivo 6:')->setRequired(true);
        $text_cz_pivo6 = new Zend_Form_Element_Text('text_cz_pivo6', array('class' => 'textboxwide', 'value' => $text[6]['text_cz']));
        $text_cz_pivo6->setLabel('Popis pivo 6:')->setRequired(true);
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));

        $this->addElements(array(
            $text_nacepu_cz,
            $text_nacepu_en,
            $title_cz_pivo1, 
            $text_cz_pivo1, 
            $title_cz_pivo2, 
            $text_cz_pivo2, 
            $title_cz_pivo3, 
            $text_cz_pivo3, 
            $title_cz_pivo4, 
            $text_cz_pivo4, 
            $title_cz_pivo5, 
            $text_cz_pivo5, 
            $title_cz_pivo6, 
            $text_cz_pivo6, 
            $submit
        ));
    }


}

