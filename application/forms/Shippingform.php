<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Shippingform extends Zend_Form {

    public function __construct() {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/admin/shipping/shippingsave/');
        $this->setAttrib('class', 'admintable');

        $setting_id = new Zend_Form_Element_Hidden('shipping_id');
        $setting_id->setDecorators(array('ViewHelper'));
        $public = new Zend_Form_Element_Radio('public');
        $public->addMultiOptions(array(
            '0' => 'Zákázáno',
            '1' => 'Umožněno',
        ));
        $public->setValue('0');
        $public->setLabel('Status:')->setRequired(true);
        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textbox'));
        $title_cz->setLabel('Název česky')->setRequired(true);
        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => 'textbox'));
        $text_cz->setLabel('Doprovodný text česky')->setRequired(true);
        $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textbox'));
        $title_en->setLabel('Název anglicky')->setRequired(true);
        $text_en = new Zend_Dojo_Form_Element_Textarea('text_en', array('class' => 'textbox'));
        $text_en->setLabel('Doprovodný text anglicky')->setRequired(true);
        $title_de = new Zend_Dojo_Form_Element_TextBox('title_de', array('class' => 'textbox'));
        $title_de->setLabel('Název německy')->setRequired(true);
        $text_de = new Zend_Dojo_Form_Element_Textarea('text_de', array('class' => 'textbox'));
        $text_de->setLabel('Doprovodný text německy')->setRequired(true);
        $this->addElements(array($setting_id, $public, $title_cz, $text_cz, $title_en, $text_en, $title_de, $text_de));
        $this->addDisplayGroup(array(
            'public',
            'title_cz',
            'text_cz',
            'title_en',
            'text_en',
            'title_de',
            'text_de'
                ), 'group', array('legend' => 'ahoj'));
        $group = $this->getDisplayGroup('group');
        $group->setDecorators(array(
            'FormElements',
            'Fieldset',
            array('HtmlTag', array('tag' => 'div'))
        ));
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "button bg-green border-style active"));
        $this->addElement($submit);
    }

}
