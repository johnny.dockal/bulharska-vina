<?php
class Default_NovinkyController extends Zend_Controller_Action {

    private $lang = null;

    function init() {
        $this->view->header = 'medium';        
    }

    function indexAction() {
        $modelNews = new Model_DbTable_News();
        $news = $modelNews->fetchNewsAll();
        
        $this->view->title = 'Aktuálně';  
        $this->view->news = $news;
    }    

    function setnewsvalueAction() {
        $modelNews = new Model_DbTable_News();
        
        // get params
        $show = $this->getParam('show');
        $value = ($show == '1') ? true : false;
        
        // edit value
        $modelNews->setNewsActiveAjax($value);
        
        // disable render
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
    }
}
?>