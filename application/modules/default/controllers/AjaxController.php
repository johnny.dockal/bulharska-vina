<?php

class Default_AjaxController extends Zend_Controller_Action {    

    public function init() {
        
    }
    
    public function addproductAction() {
        $cart = new Model_EshopCart();
        $product_id = $this->_getParam('product_id');
        $product_price = $this->_getParam('product_price');
        $quantity = $this->_getParam('product_quantity');

        $data = $cart->addProductAjax($product_id, $quantity, $product_price);
        $this->_helper->json($data);
    }
    
    public function getcategoryAction() {
        $group_id = $this->_getParam('group_id');
        $subcatId1 = $this->_getParam('subcatId1');
        $subcatId2 = $this->_getParam('subcatId2');

        $modelGroups = new Model_DbTable_EshopGroups();
        $data = $modelGroups->fetchGroupByIdAjax($group_id, $subcatId1, $subcatId2);

        $this->_helper->json($data);
    }
    
    public function emailsigninAction() {
        $model = new Model_DbTable_Mailinglist();
        $address = $this->_getParam('address');
        $captchaid = $this->_getParam('captchaid');
        $captchainput = $this->_getParam('captchainput');
        
        $captcha = array('id' => $captchaid, 'input' => $captchainput);
        
        // zkontrolujeme, zda je adresa relevantní
        if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
            $output['code'] = -1;
            $output['message'] = 'Adresa není v požadovaném formátu.';
            $this->_helper->json($output);
            return;
        }
        
        /* zkontrolujeme, zda má adresa existrující MX záznam
          if (!checkdnsrr($domain, 'MX')) {
          $status = 0;
          } */
          
        $validator = new Zend_Form_Element_Captcha('captcha', array(
            'name' => 'captcha',
            'label' => "Děkujeme!",
            'captcha' => 'Dumb',
            'captchaOptions' => array(
                'captcha' => 'Dumb',
                'wordLen' => 4,
                'timeout' => 300,
            ),
        ));
        
        if ($validator->isValid($captcha)) {
            // Validated!
            
            // zkontrolujeme, zda už náhodou není adresa v databázi
            $entry = $model->fetchRow("mail = '$address'");
            if (isset($entry)) {
                //adresa je v datab�zi, ale je odhl�en� od odb�ru
                if ($entry->status <= 0) {
                    $data = array(
                        'status' => '4'
                    );
                    $where = "mail_id = '$entry->mail_id'";
                    $model->update($data, $where);
                    
                    $output['code'] = 0;
                    $output['message'] = 'Adresa registrována. Děkujeme vám za důvěru.';
                } else {
                    $output['code'] = -2;
                    $output['message'] = 'Tuto adresu již máme v adresáři.';
                }
            } else {
                $data = array(
                    'mail' => $address,
                    'status' => 1,
                    'token' => md5($address . "a1b2c3")
                );
                $model->insert($data);
                
                $output['code'] = 0;
                $output['message'] = 'Adresa registrována. Děkujeme vám za důvěru.';
            }
        } else {
            $output['code'] = -3;
            $output['message'] = 'Captcha není správně vyplněna.';
        }
        
        $this->_helper->json($output);
    }

    public function emailsignoffAction() {
        $model = new Model_DbTable_Mailinglist();
        $address = $this->_getParam('address');
        
        if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
            $output['code'] = -1;
            $output['message'] = 'Adresa není v požadovaném formátu.';
        } else {
            $entry = $model->fetchRow("mail = '$address'");
            if (!isset($entry)) {
                $output['code'] = -2;
                $output['message'] = 'Taková adresa v adresáři není.';
            } else {
                $where = "mail = '$address'";
                $data = array(
                    'status' => '-1'
                );
                $model->update($data, $where);
                $output['code'] = 0;
                $output['message'] = 'Email byl odstraněn z adresáře.';
            }
        } 
        $this->_helper->json($output);
    }

}
