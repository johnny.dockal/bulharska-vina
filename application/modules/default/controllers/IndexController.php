<?php

class Default_IndexController extends Zend_Controller_Action {

    private $lang = null;
    private $breadcrumbs = null;

    function init() {
        $session = new Zend_Session_Namespace('Default');
        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        
        $this->breadcrumbs = array();
        //default header
        $this->view->header = 'big';
        $this->lang = $session->lang;
        $this->view->show_news = '1';
        
        if ($this->getRequest()->isPost()) {            
            $aParams = array();
            $aParams['subcatIds'] = array();
            $wine_type = $this->getParam('wine_type');
            $wine_variety = $this->getParam('wine_variety');
            $wine_viticulture = $this->getParam('wine_viticulture');
            $amount_upper = $this->getParam('amount-upper');
            $amount_lower = $this->getParam('amount-lower');
            if (!empty($wine_type)) {
                $this->view->wine_type = $wine_type;
                $session->wine_type = $wine_type;
                array_push($aParams['subcatIds'], $wine_type);
            }
            if (!empty($wine_variety)) {
                $this->view->wine_variety = $wine_variety;
                $session->wine_variety = $wine_variety;
                array_push($aParams['subcatIds'], $wine_variety);
            }
            if (!empty($wine_viticulture)) {
                $this->view->wine_viticulture = $wine_viticulture;
                $session->wine_viticulture = $wine_viticulture;
                array_push($aParams['subcatIds'], $wine_viticulture);
            }
            if (!empty($amount_upper)) {
                $this->view->amount_upper = $amount_upper;
                $aParams['amount_upper'] = $amount_upper;
            }
            if (!empty($amount_lower)) {
                $this->view->amount_lower = $amount_lower;
                if ($amount_lower == 100) {
                    $aParams['amount_lower'] = 0;
                } else {
                    $aParams['amount_lower'] = $amount_lower - 10;
                }
            }
            $session->filterParams = $aParams;
        } else if ($uri == "/filtr") {
            $this->view->wine_type = $session->wine_type;
            $this->view->wine_variety = $session->wine_variety;
            $this->view->wine_viticulture = $session->wine_viticulture;
        } else {
            $this->view->wine_type = '0';
            $this->view->wine_variety = '0';
            $this->view->wine_viticulture = '0';
        }
    }

    function indexAction() {
        $session = new Zend_Session_Namespace('Default');
        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $keyword = $this->getParam('keyword');
        $model = new Model_DbTable_EshopProducts();              

        // check session variable for news visibility
        if (isset($session->news_active) AND ! $session->news_active) {
            $this->view->show_news = '0';
        }

        if (!empty($keyword)) {
            $session->search = $keyword;
            $this->_redirect('/vyhledavani');
            return;
        }

        if ($this->getRequest()->isPost()) {            
            $this->_redirect('/filtr');
            return;
        } else if ($uri == "/filtr") {
            $this->view->title = "VÝBĚR";
            $this->view->header = 'medium';
            $this->view->products = $model->fetchProductsOfSubcategories($session->filterParams);
            return;
        } else if ($uri == "/vyhledavani") {
            if (isset($session->search)) {
                $this->view->title = "VYHLEDÁNÍ VÝRAZU: $session->search";
                $this->view->header = 'medium';
                $this->view->products = $model->fetchProductsSearch($session->search);
                return;
            } else {
                $this->_redirect('/nabidka-vin');
            }
        } else if ($uri == "/") {
            $this->view->title = "DOPORUČUJEME";
            //86 kategorie doporučujeme
            $this->view->products = $model->fetchProductsBySubcat(86);
            $this->view->products2 = $model->fetchProductsBycat(19, null, null, 86);
            $this->view->showNewsTitle = true;
            $this->view->showAllWines = true;
            return;
        } else if ($uri == "/nabidka-vin") {
            $this->view->title = "NABÍDKA VÍN";
            //19 kategorie VÍNA
            $this->view->page = 'nabidka';
            $this->view->products = $model->fetchProductsBycat(19);
            $this->view->showNewsTitle = true;
            $this->view->header = 'medium';
            return;
        }

        $urls = explode("/", $uri);
        if (isset($urls[2]) && (!empty($urls[2]))) {
            $alias = $urls[2];
        } else if (isset($urls[1]) && (!empty($urls[1]))) {
            $alias = $urls[1];
        }
        $product = $model->fetchProductByAlias($alias);
        if (!empty($product) && isset($urls[2]) && !empty($urls[2])) {
            $view = 'subcatproduct';            
        } else if (!empty($product)) {
            $view = 'product';
        } else {
            $view = 'subcat';
        }
        switch ($view) {
            case 'subcatproduct':
            case 'product':
                $subcat_ids = explode(';', $product->getSubcategoryIds());
                $this->view->title = $product->getFullTitle();
                $this->view->product = $product;
                $this->view->products = $model->fetchProductsBySubcat($subcat_ids[0], null, 4, $product->getProductId());
                $this->view->showAllWines = true;
                $this->view->header = 'medium';
                $this->view->page = 'nabidka';
                $subcatwinetype = $product->getWineType();
                $this->breadcrumbs[0]['alias'] = 'nabidka-vin';
                $this->breadcrumbs[0]['title'] = 'Nabídka vín';
                $this->breadcrumbs[1]['alias'] = $subcatwinetype['alias'];
                $this->breadcrumbs[1]['title'] = $subcatwinetype['title'];
                $this->breadcrumbs[2]['title'] = $product->getTitle();
                $this->breadcrumbs[2]['alias'] = $product->getAlias();
                $this->view->breadcrumbs = $this->breadcrumbs;
                $this->render('product');
                break;
            case 'subcat':
                $subcatModel = new Model_DbTable_EshopSubCategories();
                $subcategory = $subcatModel->fetchSubCategoryByAlias($urls[1]);
                $this->view->wine_type = $subcategory->subcategory_id;
                $this->view->subcategory = $subcategory;
                $this->view->title = $subcategory->title;
                $this->view->products = $model->fetchProductsBySubcat($subcategory->subcategory_id);
                $this->view->products2 = $model->fetchProductsBycat(19, null, null, $subcategory->subcategory_id);
                $this->view->header = 'medium';
                $this->view->page = 'nabidka';
                $this->breadcrumbs[0]['alias'] = 'nabidka-vin';
                $this->breadcrumbs[0]['title'] = 'Nabídka vín';
                $this->breadcrumbs[1]['title'] = $subcategory->title;
                $this->breadcrumbs[1]['alias'] = $subcategory->alias;
                $this->view->breadcrumbs = $this->breadcrumbs;
                break;
        }
        //throw new Zend_Controller_Action_Exception('This page does not exist', 404);
    }

    public function saveAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        $address = $this->_request->getPost('address');
        $redirect = $this->_request->getPost('redirect');
        if (isset($address)) {
            $status = 1;
        } else {
            $address = $this->_request->getQuery('address');
            $token = $this->_request->getQuery('token');
            if ($token == md5($address . "a1b2c3")) {
                $status = 3;
            } else {
                $status = 0;
            }
        }
        //zkontrolujeme, zda je adresa relevantní
        if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
            $this->view->message = "<h2>Adresa $address Není ve správném formátu.</h2>";
            $status = 0;
        }
        /* /*zkontrolujeme, zda má adresa existrující MX záznam
          if (!checkdnsrr($domain, 'MX')) {
          $status = 0;
          } */
        if ($status > 0) {
            //zkontrolujeme, zda už náhodou není adresa v databázi
            $entry = $mailmodel->fetchRow("mail = '$address'");
            if (isset($entry)) {
                //adresa je v databázi, ale je odhlášená od odběru
                if ($entry->status <= 0) {
                    $data = array(
                        'status' => '4'
                    );
                    $where = "mail_id = '$entry->mail_id'";
                    $mailmodel->update($data, $where);
                    $this->view->message = "<h2>Adresa $address úspěšně registrována. Děkujeme vám za důvěru..</h2>";
                } else {
                    $this->view->message = "<h2>Tuto adresu ($address) již máme v databázi.</h2>";
                }
            } else {
                $data = array(
                    'eshop_id' => APP_ID,
                    'mail' => $address,
                    'status' => $status,
                    'token' => md5($address . "a1b2c3")
                );
                $mailmodel->insert($data);
                $this->view->message = "<h2>Adresa $address úspěšně registrována. Děkujeme vám za důvěru.</h2>";
            }
        }
        $this->render('save');
        //$this->_redirect($redirect);
    }

}

?>