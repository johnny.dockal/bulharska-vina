<?php
/*
 * ErrorController
 *
 * Chybove hlaseni
 *
 * @package default
 * @author Martin Svec
 * @version 2010
 * @access public
 */
class Default_ErrorController extends Zend_Controller_Action
{
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        $layout = Zend_Layout::getMvcInstance()->disableLayout();
        switch($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Stránka nebyla nalezena';
                break;
            default:
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Chyba v aplikaci';
                break;
        }
        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;
    }
}

