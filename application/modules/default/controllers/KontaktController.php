<?php

class Default_KontaktController extends Zend_Controller_Action {

    private $lang = null;

    function init() {
        $session = new Zend_Session_Namespace('Default');
        $this->lang = $session->lang;      
        $this->view->header = 'small';  
        $this->view->title = "KONTAKTNÍ INFORMACE";
    }

    function indexAction() {
        $textModel = new Model_DbTable_Texts();
        $this->view->kontak = $textModel->fetchTextName('kontakt');
        $this->view->page = 'kontakt';
    }    
}
?>