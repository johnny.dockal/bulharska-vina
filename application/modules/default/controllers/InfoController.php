<?php

class Default_InfoController extends Zend_Controller_Action {

    private $lang = null;

    function init() {
        $session = new Zend_Session_Namespace('Default');
        $this->lang = $session->lang;     
        $this->view->header = 'small';   
        $this->view->title = "VŠE O NÁKUPU";   
    }

    function indexAction() {
        $textModel = new Model_DbTable_Texts();
        $this->view->podminky = $textModel->fetchTextName('podminky');
        
        $modelShipping = new Model_DbTable_EshopShipping();
        //1 = ČR, 5kg
        $this->view->shipping = $modelShipping->fetchShipping(1, 5);
        
        $modelPayments = new Model_DbTable_EshopPayments();
        $this->view->payments = $modelPayments->fetchPayments(3, 1);
        
        $this->view->page = 'onakupu';
    }    
}
?>